//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Infinite.LAPP.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmployeeDetail
    {
        public EmployeeDetail()
        {
            this.EmployeeUserMappingTables = new HashSet<EmployeeUserMappingTable>();
            this.LeaveRequests = new HashSet<LeaveRequest>();
            this.DashboardDetails = new HashSet<DashboardDetail>();
        }
    
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Department { get; set; }
        public int RoleId { get; set; }
        public int Location { get; set; }
        public int EmployeeCode { get; set; }
        public System.DateTime DateOfJoining { get; set; }
        public int ReportingManagerId { get; set; }
        public Nullable<bool> Trainee { get; set; }
        public Nullable<bool> IsReportingManager { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
        public string Designation { get; set; }
        public Nullable<bool> IsDeactiavted { get; set; }
    
        public virtual AspNetRole AspNetRole { get; set; }
        public virtual ICollection<EmployeeUserMappingTable> EmployeeUserMappingTables { get; set; }
        public virtual ICollection<LeaveRequest> LeaveRequests { get; set; }
        public virtual ICollection<DashboardDetail> DashboardDetails { get; set; }
    }
}
