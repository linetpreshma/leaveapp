//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Infinite.LAPP.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class DashboardDetail
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int EmployeeCode { get; set; }
        public System.DateTime Date { get; set; }
        public double LeavesBalanceLastMonth { get; set; }
        public double LeavesEarned { get; set; }
        public double LeavesTaken { get; set; }
        public double Shortfall { get; set; }
        public double AccuredLeaves { get; set; }
        public double LWP { get; set; }
        public double LeavesBalanceThisMonth { get; set; }
    
        public virtual EmployeeDetail EmployeeDetail { get; set; }
    }
}
