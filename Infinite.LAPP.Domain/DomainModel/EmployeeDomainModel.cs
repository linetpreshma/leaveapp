﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain.DomainModel
{
    public class EmployeeDomainModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Departments Department { get; set; }
        public int RoleId { get; set; }
        public Location Location { get; set; }
        public int EmployeeCode { get; set; }
        public System.DateTime DateOfJoining { get; set; }
        public int ReportingManagerId { get; set; }
        public bool Trainee { get; set; }
        public bool IsReportingManager { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string Designation { get; set; }
        public Nullable<bool> IsDeactiavted { get; set; }
    }

    public partial class vw_GetEmployeeListDomainModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Department { get; set; }
        public int RoleId { get; set; }
        public int Location { get; set; }
        public int EmployeeCode { get; set; }
        public System.DateTime DateOfJoining { get; set; }
        public int ReportingManagerId { get; set; }
        public Nullable<bool> Trainee { get; set; }
        public Nullable<bool> IsReportingManager { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
        public string Designation { get; set; }
        public Nullable<int> UserId { get; set; }
        public string ReportingManagerName { get; set; }
        public string Email { get; set; }
        public Nullable<bool> IsDeactiavted { get; set; }
    }

    public partial class DashboardDetailDomainModel
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int EmployeeCode { get; set; }
        public System.DateTime Date { get; set; }
        public double LeavesBalanceLastMonth { get; set; }
        public double LeavesEarned { get; set; }
        public double LeavesTaken { get; set; }
        public double Shortfall { get; set; }
        public double AccuredLeaves { get; set; }
        public double LWP { get; set; }
        public double LeavesBalanceThisMonth { get; set; }

    }
}
