﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain.DomainModel
{
   public class User
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public int RoleId { get; set; }
        public string Role { get; set; }
        public bool IsActive { get; set; }
        public string PhoneNumber { get { return string.Empty; } }
        public string Password { get; set; }    
    }

   public class AspNetUserDomainModel
   {
       public int Id { get; set; }
       public string FirstName { get; set; }
       public string LastName { get; set; }
       public string Email { get; set; }
       public string PasswordHash { get; set; }
       public string SecurityStamp { get; set; }
       public bool TwoFactorEnabled { get; set; }
       public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
       public bool LockoutEnabled { get; set; }
       public Nullable<int> AccessFailedCount { get; set; }
       public string UserName { get; set; }
       public int CreatedBy { get; set; }
       public System.DateTime CreatedDate { get; set; }
       public int ModifiedBy { get; set; }
       public System.DateTime ModifiedDate { get; set; }
       public bool IsDeleted { get; set; }
       public bool IsActive { get; set; }
       public Nullable<bool> EmailConfirmed { get; set; }
       public string PhoneNumber { get; set; }
       public Nullable<bool> PhoneNumberConfirmed { get; set; }
   }
}
