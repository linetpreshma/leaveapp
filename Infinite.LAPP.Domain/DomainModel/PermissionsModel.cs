﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain.DomainModel
{
    public class Permission
    {
        public int RoleId { get; set; }
        public List<Module> Modules { get; set; }
        public bool IsTrainee { get; set; }
        public bool IsReportingManager { get; set; }
        public string RoleName { get; set; }
        public string RoleDisplayName { get; set; }
        public string RoleDescription { get; set; }
        public bool IsPreDefinedRole { get; set; }
    }

    public class Role
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public int UserCount { get; set; }
        public string RoleDescription { get; set; }
    }

    public class AddEmployeeRoleDomainModel : Role
    {
        public bool IsTrainee { get; set; }
        public bool IsReportingManager { get; set; }
    }
    public class Module
    {
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string AccessRight { get; set; }
        public List<string> Claims { get; set; }

        public List<Tuple<string, string>> GetAuthorizedClaims()
        {
            List<Tuple<string, string>> claims = new List<Tuple<string, string>>();

            //if rights = readwrite, then all claims can be assigned
            if (this.AccessRight == "ReadWrite")
            {
                foreach (var c in Claims)
                    claims.Add(new Tuple<string, string>(this.ModuleName, c));
            }

            //if rights is read only, then only assign view claims
            else if (this.AccessRight == "ReadOnly")
            {
                foreach (var c in Claims.Where(c => c.Contains("View")))
                    claims.Add(new Tuple<string, string>(this.ModuleName, c));
            }

            //else if access rights is denied, dont assign any claims

            return claims;
        }
    }

    public class UserRolesModel
    {
        public int UserId;
        public List<int> Roles;
    }

}
