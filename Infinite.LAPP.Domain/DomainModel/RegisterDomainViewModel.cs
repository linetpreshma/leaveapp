﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain.DomainModel
{
    public class RegisterDomainModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
       
        public string Password { get; set; }

        public string PasswordHash { get; set; }

        public string ConfirmPassword { get; set; }

        public string SecurityStamp { get; set; }
    }
}
