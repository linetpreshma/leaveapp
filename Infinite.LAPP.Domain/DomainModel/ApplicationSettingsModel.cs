﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain.DomainModel
{
    public class ApplicationSettingsModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }

    public class EmailApplicationSettingsModel
    {
        public ApplicationSettingsModel AccountName { get; set; }

        public ApplicationSettingsModel Password { get; set; }

        public ApplicationSettingsModel SenderEmailAddress { get; set; }

        public ApplicationSettingsModel SenderDisplayName { get; set; }

        public ApplicationSettingsModel Token { get; set; }
    }
}
