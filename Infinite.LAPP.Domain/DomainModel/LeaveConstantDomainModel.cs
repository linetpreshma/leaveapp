﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain.DomainModel
{
    public class LeaveConstantDomainModel
    {
        public int Id { get; set; }
        public System.DateTime Date { get; set; }
        public string Location { get; set; }
        public int WorkingDaysOnProbation { get; set; }
        public int WorkingDaysNonProbation { get; set; }
    }
}
