﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain.DomainModel
{
  public  class LeaveRequestDomainModel
    {
        public int ID { get; set; }
        public int EmployeeId { get; set; }
        public Nullable<System.DateTime> FromDate { get; set; }
        public Nullable<System.DateTime> ToDate { get; set; }
        public Category Category { get; set; }
        public LeaveType LeaveType { get; set; }
        public List<DatesDomainModel> DayList { get; set; }
        public string ApplierComment { get; set; }
        public int Status { get; set; }
        public int ReportingManagerId { get; set; }
    }

  public class DatesDomainModel
  {
      public int Id { get; set; }
      public DateTime Date { get; set; }
      public Duration Duration { get; set; }
      public int Status { get; set; }
      public Nullable<bool> IsDeleted { get; set; }
  }

  public class EditLeaveDomainModel
  {
      public int Id { get; set; }
      public int EmployeeId { get; set; }
      public List<DatesDomainModel> DayList { get; set; }
      public Category Category { get; set; }
      public int EmployeeCode { get; set; }
      public string EmployeeName { get; set; }
      public string ApproverComment { get; set; }
      public string ApplierComment { get; set; }
      public string ApproverName { get; set; }
      public LeaveType Type { get; set; }
     
      public System.DateTime FromDate { get; set; }
      
      public System.DateTime ToDate { get; set; }
      public int Status { get; set; }
      public int ReportingManagerId { get; set; }
  }
}
