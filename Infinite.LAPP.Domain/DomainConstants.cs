﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain
{
    public enum Duration
    {
        Half = 1,
        Full = 2
    }

    public enum LeaveType
    {
        Planned = 1,
        Unplanned = 2
    }

    public enum Category
    {
        Sick = 1,
        Personal = 2,
        [Display(Name = "Personal Emergency")]
        PersonalEmergency = 3,
        Floating = 4,
        Maternity = 5,
        Casual = 6,
        vacation=7
    }


    public enum Location
    {
        [Display(Name = "CDR-Cedar Rapids")]
        CedarRapids = 1,
        [Display(Name = "MDC-Mumbai")]
        Mumbai = 2,
        Thailand = 3,
        Singapore = 4
    }

    public class EmailAppSettingsConstants
    {
        public const int SendGridAccountName_Id = 1;
        public const int SendGridAccountPassword_Id = 2;
        public const int SenderEmailAddress_Id = 3;
        public const int SenderDisplayName_Id = 4;
        public const int AdminEmail_Id = 5;
        public const int EmployeeRegistrationEmailTemplate_Id = 7;
        public const int LeaveApplicationEmailTemplate_Id = 8; 
        public const int LeaveApplicationStatusEmailTemplate_Id = 9;

    }
    public class DomainConstants
    {
        public const string SuperUserRole = "SuperUser";
        public const string Employee = "Employee";
        public const string Manager = "Manager";
        public const string Commonpassword = "Infinite123#";
        public const int AdminEmailAddress = 5;
    }

    public enum Departments
    {
        Management = 1,
        Recruitment = 2,
        Projects = 3,
        Systems = 4,
        Accounts = 5,
        HR = 6

    }
    public enum EmployeeRole
    {
        Trainee = 1,
        SrDeveploper = 2
    }

}
