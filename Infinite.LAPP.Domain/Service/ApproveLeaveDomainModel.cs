﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infinite.LAPP.Domain.DomainModel
{
    public class ApproveLeaveDomainModel
    {
        public int LeaveRequestId { get; set; }
        public int EmployeeId { get; set; }
        public List<DatesDomainModel> DayList { get; set; }
        public string ApproverComment { get; set; }
        public int Status{ get; set; }
    }
}
