﻿using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain.Service
{
    public class EmailApplicationSettingsService
    {
        private readonly ISettingsRepository _settingsRep;

        public EmailApplicationSettingsService(ISettingsRepository settingsRep)
        {
            if (settingsRep == null)
                throw new ArgumentNullException("settingsRep is null");

            this._settingsRep = settingsRep;
        }

        public EmailApplicationSettingsModel GetEmailSettings(int templateId)
        {
            var accName = _settingsRep.GetById(EmailAppSettingsConstants.SendGridAccountName_Id);
            var accPassword = _settingsRep.GetById(EmailAppSettingsConstants.SendGridAccountPassword_Id);
            var senderEmail = _settingsRep.GetById(EmailAppSettingsConstants.SenderEmailAddress_Id);
            var senderName = _settingsRep.GetById(EmailAppSettingsConstants.SenderDisplayName_Id);

            var token = _settingsRep.GetById(templateId);

            var model = new EmailApplicationSettingsModel()
            {
                AccountName = accName,
                Password = accPassword,
                SenderEmailAddress = senderEmail,
                SenderDisplayName = senderName,
                Token = token
            };

            return model;
        }

        public string GetAdminEmailAddress()
        {
            return _settingsRep.GetById(DomainConstants.AdminEmailAddress).Value;
        }
    }
}
