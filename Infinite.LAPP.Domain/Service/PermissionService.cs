﻿using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain.Service
{
    public class PermissionService
    {
        private readonly IPermissionRepository _permissionsRep;
        private readonly IUserRolesRepository _userRolesRep;

        public PermissionService(IPermissionRepository permissionsRep, IUserRolesRepository userRolesRep)
        {
            if (permissionsRep == null)
                throw new ArgumentNullException("permissionsRep is null");
            if (userRolesRep == null)
                throw new ArgumentNullException("rolesRep is null");

            this._permissionsRep = permissionsRep;
            this._userRolesRep = userRolesRep;
        }

        public List<Tuple<string, string>> GetClaims(int userId)
        {
            List<Tuple<string, string>> claims = new List<Tuple<string, string>>();

            foreach (var r in _userRolesRep.GetById(userId).Roles)
            {
                //get permission settings for role
                Permission permissions = _permissionsRep.GetById(r);

                //fetch claims for each module and filter them based on access right assigned to the module
                foreach (var p in permissions.Modules)
                {
                    //add claims to the claim list
                    claims.AddRange(p.GetAuthorizedClaims());
                }
            }

            return claims;
        }
    }
}
