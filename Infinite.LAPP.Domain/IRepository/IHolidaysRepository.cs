﻿using Infinite.LAPP.Domain.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain.IRepository
{
    public interface IHolidaysRepository : IRepository<HolidaysDomainModel>
    {
        void DeleteHoliday(int Id);

        void UpdateHoliday(HolidaysDomainModel domainModel);
      
    }
}
