﻿using Infinite.LAPP.Domain.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain.IRepository
{
    public interface ILeaveRepository : IRepository<LeaveRequestDomainModel>
    {
        int ApproveRequest(ApproveLeaveDomainModel domainModel);
        int RejectLeave(int Id, string ApproverComment);
        int GetReportingManagerId(int employeeId);
        void EditLeave(EditLeaveDomainModel domainModel);
        void CancelLeave(int LeaveRequestId);
        void UpdateReportingManagerForLeaveRequest(int EmployeeId,int NewReportingManagerId);
        int GetEmployeeIdByLeaveId(int LeaveId);
        LeaveRequestDomainModel GetLeaveDetailsById(int LeaveId);
    }
}
