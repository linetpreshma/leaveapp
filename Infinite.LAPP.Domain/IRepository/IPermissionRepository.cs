﻿using Infinite.LAPP.Domain.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain.IRepository
{
    public interface IPermissionRepository : IReadOnlyRepository<Permission>
    {

    }

    public interface IUserRolesRepository : IReadOnlyRepository<UserRolesModel>
    {

    }
    public interface IRoleRepository : IReadOnlyRepository<Role>
    {

    }

    public interface IModuleRepository : IReadOnlyRepository<Module>
    {

    }

    public interface IRolePermissionRepository : IRepository<Permission>
    {
        bool IsUniqueRoleName(string roleName, int roleId);

        bool IsUniqueDisplayRoleName(string roleDisplayName, int roleId);
    }
}
