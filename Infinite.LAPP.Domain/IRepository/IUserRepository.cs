﻿using Infinite.LAPP.Domain.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain.IRepository
{
    public interface IUserRepository : IRepository<User>
    {

        void UpdateProfile(User user, int loggedInUserId);

        List<Role> GetAllRoles();

        bool IsEmailAvailable(string email);

        string GetHashedPassword(int userId);
        List<AddEmployeeRoleDomainModel> GetAllRolesForEmployee();

        AspNetUserDomainModel GetUserByUserName(string UserName);
    }
}
