﻿using Infinite.LAPP.Domain.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain.IRepository
{
    public interface IEmployeeRepository : IRepository<EmployeeDomainModel>
    {

         //List<EmployeeDomainModel> GetEmployeeByLocation(int locationId);
         void InsertInEmployeeMappingTable(int UserId, int EmployeeId);
         //EmployeeDomainModel GetEmployeeDetailByUserId(int UserId);

         bool IsEmployeeCodeUnique(int EmployeeCode);

         void DeactivateEmployeeById(int empId);

         void UpdateEmployee(int EmployeeId, int ReportingManagerId);

         EmployeeDomainModel GetEmployeeById(int Id);

         vw_GetEmployeeListDomainModel GetEmployeeDetailByUserId(int UserId);

         double GetTotalHours(int EmployeeCode);

         LeaveConstantDomainModel GetLeaveConstantForCurrentMonth();

         double GetConfigConstantById(int Id);

         void InsertInDashboardDetails(DashboardDetailDomainModel Model);

         double GetLeavesBalanceLastMonth(int EmployeeId);
    }
}
