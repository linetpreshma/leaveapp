﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain.IRepository
{
    public interface IRepository<T> : IReadOnlyRepository<T>
        where T : class
    {
        int Insert(T model);

        void Update(T model);

        void Delete(T model);

        void SoftDelete(int id);
    }


    public interface IReadOnlyRepository<T>
        where T : class
    {
        IEnumerable<T> GetAll();

        T GetById(int id);

        bool IsValueUnique(string propertyName, object value, int idToExclude = 0);
    }
}
