﻿using Infinite.LAPP.Domain.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Domain.IRepository
{
    public interface ISettingsRepository : IRepository<ApplicationSettingsModel>
    {
        void UpdateEmailSettings(List<ApplicationSettingsModel> model);
    }
}
