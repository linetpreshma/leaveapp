﻿using Infinite.LAPP.Domain.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.Repository.Dependencies;
using Infinite.LAPP.Domain.IRepository;
using Infinite.LAPP.Domain;
using System.Data.Entity;
using Infinite.LAPP.ATTER.EntityModel;

namespace Infinite.LAPP.Repository
{
    public class UserRepository : BaseRepository<User, AspNetUser>,
        IUserRepository
    {
        public UserRepository(LAPPEntities context, IMapper mapper, IUserSession user, ATTEREntities contextAtter)
            : base(context, mapper, user, contextAtter)
        {

        }

        
        private AspNetRole GetRoleIdForSuperUserRole()
        {
            return _context.AspNetRoles.FirstOrDefault(x => x.Name.Equals(DomainConstants.SuperUserRole));
        }

        //private AspNetUser CreateUser(User domainModel, int superUserRoleId)
        //{
        //    AspNetUser aspNetUser = new AspNetUser()
        //    {

        //        CreatedBy = _user.UserId,
        //        CreatedDate = _user.DateTime,
        //        ModifiedBy = _user.UserId,
        //        ModifiedDate = _user.DateTime,
        //        SecurityStamp = Guid.NewGuid().ToString(),
        //        IsActive = true,
        //        IsDeleted = false,
        //        UserName = domainModel.EmailAddress,
        //        PasswordHash = domainModel.PasswordHash,
        //        LockoutEnabled = true,
        //        EmailConfirmed = true,
        //        PhoneNumber = string.Empty,
        //        TwoFactorEnabled = false,
        //        PhoneNumberConfirmed = true,
        //        LastName = domainModel.LastName,
        //        FirstName = domainModel.FirstName,
        //        AccessFailedCount = 0,
        //        Email = domainModel.EmailAddress,
        //        AspNetUserRoles = new List<AspNetUserRole>()

        //    };
        //    aspNetUser.AspNetUserRoles.Add(new AspNetUserRole()
        //    {
        //        CreatedBy = _user.UserId,
        //        CreatedDate = _user.DateTime,
        //        ModifiedBy = _user.UserId,
        //        ModifiedDate = _user.DateTime,
        //        UserId = aspNetUser.Id,
        //        RoleId = superUserRoleId
        //    });

        //    return aspNetUser;
        //}

        public AspNetUserDomainModel GetUserByUserName(string UserName)
        {
            var data = _mapper.Map<AspNetUser, AspNetUserDomainModel>(_context.AspNetUsers.FirstOrDefault(x => x.UserName == UserName));
            return data;
        }


        protected override IEnumerable<AspNetUser> GetAllQuery()
        {
            return this._context.AspNetUsers.Include(m => m.AspNetUserRoles.Select(p => p.AspNetRole));
        }

        public override void Update(Domain.DomainModel.User u)
        {
            int loggedInUserId = 0;
            this._context.usp_UpdateUsers(u.UserId, u.FirstName, u.LastName, u.IsActive, loggedInUserId, u.RoleId, DateTime.Now);
        }

        public void UpdateProfile(Domain.DomainModel.User user, int loggedInUserId)
        {
            AspNetUser entityUser = base.GetByIdQuery(loggedInUserId);

            entityUser.FirstName = user.FirstName;
            entityUser.LastName = user.LastName;
            entityUser.ModifiedBy = loggedInUserId;
            entityUser.ModifiedDate = DateTime.Now;

            _context.Entry(entityUser).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public List<Domain.DomainModel.Role> GetAllRoles()
        {
            List<AspNetRole> roleEntity = _context.Set<AspNetRole>().ToList();
            List<Domain.DomainModel.Role> roleModel = new List<Domain.DomainModel.Role>();

            foreach (var entity in roleEntity)
            {
                Domain.DomainModel.Role role = new Domain.DomainModel.Role();
                role.RoleId = entity.Id;
                role.RoleName = entity.Name;

                roleModel.Add(role);
            }
            return roleModel;
        }

        public List<AddEmployeeRoleDomainModel> GetAllRolesForEmployee()
        {
            List<AspNetRole> roleEntity = _context.Set<AspNetRole>().ToList();
            List<AddEmployeeRoleDomainModel> roleModel = new List<AddEmployeeRoleDomainModel>();

            foreach (var entity in roleEntity)
            {
                AddEmployeeRoleDomainModel role = new AddEmployeeRoleDomainModel();
                role.RoleId = entity.Id;
                role.RoleName = entity.DisplayName;
                role.IsTrainee = entity.IsTrainee;
                role.IsReportingManager = entity.IsReportingManager;

                roleModel.Add(role);
            }
            return roleModel;
        }

        public bool IsEmailAvailable(string email)
        {
            int user = _context.Set<AspNetUser>().Where(m => m.Email == email).Count();
            if (user > 0)
                return false;
            return true;
        }

        protected override AspNetUser GetByIdQuery(int id)
        {
            return _context.Set<AspNetUser>().Include(e => e.AspNetUserRoles.Select(p => p.AspNetRole)).SingleOrDefault(x => x.Id == id);
        }

        public string GetHashedPassword(int userId)
        {
            return _context.Set<AspNetUser>().Find(userId).PasswordHash;
        }
    }
}
