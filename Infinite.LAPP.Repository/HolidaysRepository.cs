﻿using Infinite.LAPP.ATTER.EntityModel;
using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.Domain.IRepository;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.Repository.Dependencies;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Infinite.LAPP.Repository
{
    public class HolidaysRepository : BaseRepository<HolidaysDomainModel, Holiday>,
        IHolidaysRepository
    {
        public HolidaysRepository(LAPPEntities context, IMapper mapper, IUserSession user, ATTEREntities contextAtter)
            : base(context, mapper, user, contextAtter)
        {

        }

       public void DeleteHoliday(int Id)
        {
           var item = _context.Holidays.FirstOrDefault(x => x.ID == Id);

             item.IsDeleted = true;
             _context.Entry<Holiday>(item).State = EntityState.Modified;
             var id = _context.SaveChanges();
           
       }

       public void UpdateHoliday(HolidaysDomainModel domainModel)
        {
            var entity = _context.Holidays.FirstOrDefault(x => x.ID == domainModel.ID);

            entity.Location = domainModel.Location;
            entity.Date = domainModel.Date;
            entity.Title = domainModel.Title;
            entity.Desciption = domainModel.Desciption;
            entity.ModifiedBy = _user.UserId;
            entity.ModifiedDate = _user.DateTime;

            _context.Entry<Holiday>(entity).State = EntityState.Modified;
            var id = _context.SaveChanges();
        }


     }


    }

