﻿using Infinite.LAPP.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Repository.QueryObjects
{
   public class ListRolesQuery : BaseQuery
    {
        public ListRolesQuery(LAPPEntities context)
            : base(context)
        {
        }
        
        public List<vw_RoleUserCount> Execute()
        {
            return base._context.vw_RoleUserCount.ToList();
        }
    }
}
