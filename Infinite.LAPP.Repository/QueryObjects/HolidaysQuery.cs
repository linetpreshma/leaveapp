﻿using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Repository.QueryObjects
{
    public class HolidaysQuery
    {
         private LAPPEntities context = null;

         public HolidaysQuery(LAPPEntities _context)
        {
            if (_context == null)
            throw new ArgumentNullException("_context is null");
            context = _context;

        }

        public List<Holiday> getAllHolidays()
        {
            return context.Holidays.Where(x => x.IsDeleted != true).ToList();
        }

        public List<Holiday> GetHolidaysByLocation(int locationId,DateTime dt)
        {
            return context.Holidays.Where(x => x.Location == locationId && x.Date.Year == dt.Year && x.IsDeleted!=true).ToList();
        }

        public Holiday GetHolidaysById(int Id)
        {
            return context.Holidays.AsNoTracking().FirstOrDefault(x => x.ID == Id);
        }

    }
}
