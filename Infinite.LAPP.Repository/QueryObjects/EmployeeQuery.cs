﻿using Infinite.LAPP.Domain;
using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Infinite.LAPP.Repository.QueryObjects
{
    public class EmployeeQuery
    {
         private LAPPEntities _context = null;

         public EmployeeQuery(LAPPEntities context)
        {
            if (context == null)
            throw new ArgumentNullException("context is null");
            _context = context;

        }

        public List<Tuple<int,string>> GetReportingManagers()
        {
            List<Tuple<int, string>> result = new List<Tuple<int, string>>() { };
            _context.EmployeeDetails
               .Where(x => x.IsReportingManager == true && x.IsDeactiavted!=true).ToList()
               .ForEach(y =>
               {
                   result.Add(new Tuple<int, string>(y.Id, y.FirstName));
               });
            return result;
        }

        public string GetReportingManagerName(int managerId)
        {
            var emp = _context.EmployeeDetails.FirstOrDefault(x => x.Id == managerId);
            return emp.FirstName + " " + emp.LastName;
        }

        public List<vw_GetEmployeeList> GetAllEmployeeListDetails()
        {
            return _context.vw_GetEmployeeList.ToList();
        }

        public vw_GetEmployeeList GetEmployeeDetailByUserId(int UserId)
        {
            return _context.vw_GetEmployeeList.FirstOrDefault(x=>x.UserId == UserId);
        }

        //public EmployeeDetail GetEmployeeDetailByEmployeeId(int Id)
        //{
        //    return _context.EmployeeDetails
        //        .Include(x=>x.EmployeeUserMappingTables)
        //        .Include(x=>x.
        //        .FirstOrDefault(x => x.Id == Id);
        //}


        public EmployeeDomainModel GetEmployeeDomainModelByUserId(int UserId)
        {

            var result = (from emp in _context.EmployeeDetails
                          join userMap in _context.EmployeeUserMappingTables on emp.Id equals userMap.EmployeeId
                          where userMap.UserId == UserId
                          select new
                          {
                              FirstName = emp.FirstName,
                              LastName = emp.LastName,
                              EmployeeCode = emp.EmployeeCode,
                              Id = emp.Id,
                              Location = emp.Location
                          }).FirstOrDefault();
            EmployeeDomainModel employee = new EmployeeDomainModel()
            {
                FirstName = result.FirstName,
                LastName = result.LastName,
                EmployeeCode = result.EmployeeCode,
                Id = result.Id,
                Location = (Location)result.Location
            };
            return employee;
        }



        public DashboardDetail GetDashboardDetails(int Id)
        {
            return _context.DashboardDetails.OrderByDescending(y=>y.Date).FirstOrDefault(x => x.EmployeeId == Id);
        }


    }
}
