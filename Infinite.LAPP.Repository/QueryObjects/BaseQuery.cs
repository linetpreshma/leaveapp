﻿using Infinite.LAPP.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Repository.QueryObjects
{
   public class BaseQuery
    {
        protected readonly LAPPEntities _context;

        public BaseQuery(LAPPEntities context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            _context = context; // new CustomerDBContext(connectionString);

            _context.Configuration.ValidateOnSaveEnabled = false;
            _context.Configuration.LazyLoadingEnabled = false;
            _context.Configuration.AutoDetectChangesEnabled = false;
            _context.Configuration.ProxyCreationEnabled = false;
        }
    }
}
