﻿using Infinite.LAPP.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Infinite.LAPP.Repository.QueryObjects
{
    public class LeaveRequestQuery
    {
        private LAPPEntities _context = null;

        public LeaveRequestQuery(LAPPEntities context)
        {
            if (context == null)
                throw new ArgumentNullException("context is null");
            _context = context;

        }

        //public List<LeaveRequest> GetAllLeaveRequest()
        //{
        //    return _context.LeaveRequests
        //        .ToList();
        //}

        public List<vw_GetAllLeavesListForApproval> GetAllLeavesByUserId(int UserId)
        {
            var managerId = _context.EmployeeUserMappingTables.FirstOrDefault(y => y.UserId == UserId).EmployeeId;
            return _context.vw_GetAllLeavesListForApproval
                .Where(x => x.ReportingMangerId == managerId)
                .OrderByDescending(x=>x.CreatedDate)
                .ToList();
        }

        public LeaveRequest GetLeaveRequestById(int leaveRequestId)
        {
            return (from leaveRequest in _context.LeaveRequests
                    join leaveRecordDetails in _context.LeaveRequestDetails
                    on leaveRequest.Id equals leaveRecordDetails.LeaveRequestId
                    where (leaveRecordDetails.IsDeleted != true && leaveRequest.Id == leaveRequestId)
                    select leaveRequest).FirstOrDefault();

            //return _context.LeaveRequests
            //    .Include(x => x.LeaveRequestDetails)
            //    .Include(x => x.EmployeeDetail)
            //    .Where(x => x.Id == leaveRequestId)
            //    .FirstOrDefault();
        }

        public List<vw_GetAllLeavesList> GetLeaveRequestByEmployeeId(int EmployeeId)
        {
            //returns all leaves not only approved leaves.
            return _context.vw_GetAllLeavesList
                .Where(x => x.EmployeeId == EmployeeId && x.IsDeleted==false).ToList();
        }

        public List<vw_GetAllLeavesList> GetAllLeaveRequest()
        {
            //returns all leaves not only approved leaves.
            return _context.vw_GetAllLeavesList
                .Where(x => x.IsDeleted == false).ToList();
        }


        public List<Category> GetAllCategoriesByLeaveTypeId(int leaveTypeId)
        {
            return _context.Categories.Where(x => x.LeaveTypeId == leaveTypeId).ToList();
        }

        public List<vw_GetAllLeavesListwithDetails> GetLeaveListThisMonth(int UserId, DateTime FromDate, DateTime ToDate)
        {
           return _context.vw_GetAllLeavesListwithDetails
                .Where(x => x.UserId == UserId && x.LeaveRequestStatus==1 && x.DetailsStatus ==1 && x.Date>=FromDate && x.Date <= ToDate)
                .OrderByDescending(y=>y.Date)
                .ToList();
        }
    }
}
