﻿using Infinite.LAPP.ATTER.EntityModel;
using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.Domain.IRepository;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.Repository.Dependencies;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Repository
{
    public class RolePermissionRepository : BaseRepository<Permission, AspNetRole>, IRolePermissionRepository
    {
        private IUserSession _user;
        public RolePermissionRepository(LAPPEntities context, IMapper mapper, IUserSession user, ATTEREntities contextAtter)
            : base(context, mapper, user,contextAtter)
        {
            _user = user;
        }

        protected override AspNetRole GetByIdQuery(int id)
        {
            return _context.AspNetRoles.Include(x => x.AspNetCustomRolePermissions.Select(y => y.AspNetCustomModule)).FirstOrDefault(c => c.Id == id);
        }

        /// <summary>
        /// This method is used to update an existing role entity along with associate role permission entities.
        /// </summary>
        /// <param name="model">domain model</param>
        public override void Update(Permission model)
        {
            // Find the respective entity by Id.
            var userRoleEntity = GetByIdQuery(model.RoleId);

            if (userRoleEntity.AspNetCustomRolePermissions == null || !userRoleEntity.AspNetCustomRolePermissions.Any())
            {
                throw new Exception();
            }

            _context.Set<AspNetRole>().Attach(userRoleEntity);

            //changes related to role permission list
            if (model.Modules != null && model.Modules.Any())
            {
                //fetching role permission entities based on role id.
                var rolePermissionList = _context.Set<AspNetCustomRolePermission>().Where(rolePermission => rolePermission.RoleId == model.RoleId).ToList();

                foreach (var module in model.Modules)
                {
                    //In edit mode we will have list of role permission associated with role entity.This list contains already existing role permission entities which
                    //needs to be updated and new entities if new module is got added and user assign access rights to it.
                    var existingRolePermissionEntity = rolePermissionList.FirstOrDefault(rolePermissionEntity => rolePermissionEntity.ModuleId == module.ModuleId);

                    //if entity already exists in DB then making its domain entity state as modified with new access right.
                    if (existingRolePermissionEntity != null)
                    {
                        _context.Entry(existingRolePermissionEntity).State = EntityState.Modified;
                        _context.Entry(existingRolePermissionEntity).Entity.ModifiedBy = _user.UserId;
                        _context.Entry(existingRolePermissionEntity).Entity.ModifiedDate = _user.DateTime;
                        _context.Entry(existingRolePermissionEntity).Entity.AspNetRole = null;//setting parent entity as null do avoid circular reference.

                        // Update column values.
                        _context.Entry(existingRolePermissionEntity).Entity.AccessLevel = module.AccessRight;
                    }
                }
            }

            //setting domain entity state of custom identity role entity.
            _context.Entry(userRoleEntity).State = EntityState.Modified;

            //Updating column values for custom identity role entity.
            _context.Entry(userRoleEntity).Entity.Name = model.RoleName;
            _context.Entry(userRoleEntity).Entity.DisplayName = model.RoleDisplayName;
            _context.Entry(userRoleEntity).Entity.Description = model.RoleDescription;
            _context.Entry(userRoleEntity).Entity.ModifiedBy = _user.UserId;
            _context.Entry(userRoleEntity).Entity.ModifiedDate = _user.DateTime;
            _context.SaveChanges();
        }

        /// <summary>
        /// This method is used to check whether the role name entered by user is unique or not.
        /// </summary>
        /// <param name="roleName"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public bool IsUniqueRoleName(string roleName, int roleId)
        {
            var role = _context.AspNetRoles.FirstOrDefault(x => x.Name.Equals(roleName) && x.Id != roleId);
            return role == null;
        }

        /// <summary>
        /// This method is used to check whether the role display name entered by user is unique or not.
        /// </summary>
        /// <param name="roleDisplayName"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public bool IsUniqueDisplayRoleName(string roleDisplayName, int roleId)
        {
            var role = _context.AspNetRoles.FirstOrDefault(x => x.DisplayName.Equals(roleDisplayName) && x.Id != roleId);
            return role == null;
        }

    }
}
