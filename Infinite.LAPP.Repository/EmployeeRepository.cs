﻿using Infinite.LAPP.ATTER.EntityModel;
using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.Domain.IRepository;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.Repository.Dependencies;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Repository
{
    public class EmployeeRepository : BaseRepository<EmployeeDomainModel, EmployeeDetail>,
        IEmployeeRepository
    {

        public EmployeeRepository(LAPPEntities context, IMapper mapper, IUserSession user, ATTEREntities contextAtter)
            : base(context, mapper, user, contextAtter)
        {

        }

        //public List<EmployeeDomainModel> GetEmployeeByLocation(int locationId)
        //{
        //    return _mapper.Map<List<EmployeeDetail>, List<EmployeeDomainModel>>(_context.EmployeeDetails.Where(x => x.Location == locationId).ToList());
        //}

        public void InsertInEmployeeMappingTable(int UserId, int EmployeeId)
        {
            var entity = new EmployeeUserMappingTable()
            {
                UserId = UserId,
                EmployeeId = EmployeeId
            };

            _context.Set<EmployeeUserMappingTable>().Add(entity);
            _context.Entry<EmployeeUserMappingTable>(entity).Property("CreatedDate").CurrentValue = _user.DateTime;
            _context.Entry<EmployeeUserMappingTable>(entity).Property("CreatedBy").CurrentValue = _user.UserId;
            _context.Entry<EmployeeUserMappingTable>(entity).Property("ModifiedDate").CurrentValue = _user.DateTime;
            _context.Entry<EmployeeUserMappingTable>(entity).Property("ModifiedBy").CurrentValue = _user.UserId;
            _context.Entry<EmployeeUserMappingTable>(entity).Property("IsDeleted").CurrentValue = false;

            int i = _context.SaveChanges();

            if (i == 0)
                throw new Exception("Database update failed.");

        }

        public bool IsEmployeeCodeUnique(int EmployeeCode)
        {
            var code = _context.EmployeeDetails.Where(x => x.EmployeeCode == EmployeeCode && !x.IsDeleted).FirstOrDefault();
            return code == null;
        }

        public void DeactivateEmployeeById(int empId)
        {
            int UserId = _context.EmployeeUserMappingTables.FirstOrDefault(x => x.EmployeeId == empId).UserId;
            var user = _context.AspNetUsers.Where(x => x.Id == UserId).FirstOrDefault();
            user.IsActive = false;
            _context.Entry<AspNetUser>(user).State = EntityState.Modified;

            var employee = _context.EmployeeDetails.Where(x => x.Id == empId).FirstOrDefault();

            employee.IsDeactiavted = true;
            employee.ModifiedBy = _user.UserId;
            employee.ModifiedDate = _user.DateTime;
            _context.Entry<EmployeeDetail>(employee).State = EntityState.Modified;

            var id = _context.SaveChanges();
        }

        public void UpdateEmployee(int EmployeeId, int ReportingManagerId)
        {
            var employee = _context.EmployeeDetails.Where(x => x.Id == EmployeeId).FirstOrDefault();


            employee.ReportingManagerId = ReportingManagerId;
            employee.ModifiedBy = _user.UserId;
            employee.ModifiedDate = _user.DateTime;
            _context.Entry<EmployeeDetail>(employee).State = EntityState.Modified;

            var id = _context.SaveChanges();
            
        }

        public EmployeeDomainModel GetEmployeeById(int Id)
        {
            return _mapper.Map<EmployeeDetail, EmployeeDomainModel>
                (_context.EmployeeDetails.AsNoTracking().FirstOrDefault(x => x.Id == Id));
                
        }


        public vw_GetEmployeeListDomainModel GetEmployeeDetailByUserId(int UserId)
        {
            return _mapper.Map<vw_GetEmployeeList, vw_GetEmployeeListDomainModel>(_context.vw_GetEmployeeList.FirstOrDefault(x => x.Id == UserId));
             
        }

        public double GetTotalHours(int EmployeeCode)
        {
            var date = DateTime.Now.AddMonths(-1);
            var lastdate = DateTime.DaysInMonth(date.Year, date.Month);

            //DateTime fromdate = new DateTime(2017, 1, 1);
            //DateTime todate = new DateTime(2017, 1,31 );

            DateTime fromdate = new DateTime(date.Year, date.Month, 1);
            DateTime todate = new DateTime(date.Year, date.Month,lastdate);


            var temp= (from atter in _contextAtter.vw_LAPP
                       where atter.EmployeeCode == EmployeeCode.ToString() && atter.AttendanceDate >= fromdate && atter.AttendanceDate <= todate
                       select new { atter.EmployeeCode, atter.Duration, atter.Absent }).ToList();            
            double sum = temp.Select(c=>c.Duration).Sum();

            return sum;
        }

        public LeaveConstantDomainModel GetLeaveConstantForCurrentMonth()
        {
            return _mapper.Map<LeaveConstant, LeaveConstantDomainModel>
               (_context.LeaveConstants.FirstOrDefault(x=>x.Date.Month == DateTime.Now.Month));
        }


        public double GetConfigConstantById(int id)
        {

            var value = _context.ApplicationSettings.FirstOrDefault(x => x.Id == id).Value;

            return Convert.ToDouble(value);

        }

        public void InsertInDashboardDetails(DashboardDetailDomainModel Model)
        {
            var entity = _mapper.Map<DashboardDetailDomainModel, DashboardDetail>(Model);

            _context.Set<DashboardDetail>().Add(entity);

            int i = _context.SaveChanges();

            if (i == 0)
                throw new Exception("Database update failed.");
        
        }

        public double GetLeavesBalanceLastMonth(int EmployeeId)
        {
            double bal = _context.DashboardDetails.OrderByDescending(x=>x.Date).FirstOrDefault(x => x.EmployeeId == EmployeeId).LeavesBalanceThisMonth;

            return bal;
        }


    }
}
