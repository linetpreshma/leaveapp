﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infinite.LAPP.Repository.Dependencies
{
    public interface IMapper
    {
        T2 Map<T1, T2>(T1 obj);

        List<T2> Map<T1, T2>(List<T1> obj);

        string GetCorrespondingPropertyName<T1, T2>(string propertyName);
    }
}
