﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Repository.Dependencies
{
    public interface IUserSession
    {
        int UserId { get; }
        DateTime DateTime { get; }
    }
}
