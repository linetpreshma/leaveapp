﻿using Infinite.LAPP.Domain.IRepository;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.Repository.Dependencies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Infinite.LAPP.ATTER.EntityModel;

namespace Infinite.LAPP.Repository
{
    public class UserRolesRepository : BaseReadOnlyRepository<Domain.DomainModel.UserRolesModel, EntityModel.AspNetUser>,
       IUserRolesRepository
    {
        public UserRolesRepository(LAPPEntities context, IMapper mapper, ATTEREntities contextAtter)
            : base(context, mapper,contextAtter)
        {

        }

        protected override AspNetUser GetByIdQuery(int id)
        {
            return
                _context.AspNetUsers.Include(a => a.AspNetUserRoles)
                    .Include(a => a.AspNetUserRoles.Select(y => y.AspNetRole))
                    .First(c => c.Id == id);
        }

        protected override Domain.DomainModel.UserRolesModel ToDomainModel(AspNetUser entity)
        {
            var model = new Domain.DomainModel.UserRolesModel();
            model.UserId = entity.Id;

            model.Roles = new List<int>();
            foreach (var i in entity.AspNetUserRoles)
            {
                model.Roles.Add(i.RoleId);
            }

            return model;
        }
    }
}
