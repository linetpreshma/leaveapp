﻿using Infinite.LAPP.ATTER.EntityModel;
using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.Domain.IRepository;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.Repository.Dependencies;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Repository
{
    public class SettingsRepository : BaseRepository<ApplicationSettingsModel, EntityModel.ApplicationSetting>, ISettingsRepository
    {
        public SettingsRepository(LAPPEntities context, IMapper mapper, IUserSession user, ATTEREntities contextAtter)
            : base(context, mapper, user,contextAtter)
        {

        }

        public virtual void UpdateEmailSettings(List<ApplicationSettingsModel> models)
        {
            var emailEntities = _context.ApplicationSettings.Where(m => m.Type == 0 || m.Type == 1).ToList();

            foreach (var entity in emailEntities)
            {
                var model = models.Find(m => m.Id == entity.Id);

                if (model != null)
                {
                    entity.Value = model.Value;
                    entity.ModifiedBy = _user.UserId;
                    entity.ModifiedDate = _user.DateTime;

                    base._context.Entry<EntityModel.ApplicationSetting>(entity).State = EntityState.Modified;
                }
            }

            int rowsAffected = base._context.SaveChanges();

            if (rowsAffected <= 0)
                throw new Exception("Database updating failed. No row to update.");
        }
    }
}
