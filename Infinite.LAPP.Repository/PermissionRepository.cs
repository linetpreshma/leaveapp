﻿using Infinite.LAPP.Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.Repository.Dependencies;
using System.Data.Entity;
using Infinite.LAPP.ATTER.EntityModel;

namespace Infinite.LAPP.Repository
{
    public class PermissionRepository : BaseReadOnlyRepository<Domain.DomainModel.Permission, EntityModel.AspNetRole>,
      IPermissionRepository
    {
        public PermissionRepository(LAPPEntities context, IMapper mapper, ATTEREntities contextAtter)
            : base(context, mapper,contextAtter)
        {

        }

        protected override IEnumerable<AspNetRole> GetAllQuery()
        {
            return _context.AspNetRoles.Include(a => a.AspNetCustomRolePermissions.Select(b => b.AspNetCustomModule).Select(c => c.AspNetCustomClaims));
        }

        protected override AspNetRole GetByIdQuery(int id)
        {
            return _context.AspNetRoles.Include(a => a.AspNetCustomRolePermissions.Select(b => b.AspNetCustomModule).Select(b => b.AspNetCustomClaims)).First(c => c.Id == id);
        }

    }
}
