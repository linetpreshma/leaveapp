﻿using Infinite.LAPP.ATTER.EntityModel;
using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.Domain.IRepository;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.Repository.Dependencies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinite.LAPP.Repository
{
    public class ModuleRepository : BaseReadOnlyRepository<Module, EntityModel.AspNetCustomModule>, IModuleRepository
    {
        public ModuleRepository(LAPPEntities context, IMapper mapper, ATTEREntities contextAtter)
            : base(context, mapper, contextAtter)
        {

        }

        protected override Module ToDomainModel(EntityModel.AspNetCustomModule entity)
        {
            return new Module { ModuleId = entity.ModuleId, ModuleName = entity.Name };
        }

    }
}
