﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infinite.LAPP.EntityModel;
using System.Linq.Expressions;
using System.Reflection;
using Infinite.LAPP.Repository.Dependencies;
using System.Data.Entity;
using Infinite.LAPP.ATTER.EntityModel;

namespace Infinite.LAPP.Repository
{
    public abstract class BaseReadOnlyRepository<M, E>
        where M : class
        where E : class
    {
        protected readonly LAPPEntities _context;
        protected readonly ATTEREntities _contextAtter; 
        protected readonly IMapper _mapper;
        protected const string MODIFIED_BY = "ModifiedBy";
        protected const string MODIFIED_DATE = "ModifiedDate";
        protected const string CREATED_BY = "CreatedBy";
        protected const string CREATED_DATE = "CreatedDate";
        protected const string IS_DELETED = "IsDeleted";

        public BaseReadOnlyRepository(LAPPEntities context, IMapper mapper, ATTEREntities contextAtter)
        {
            if (context == null)
                throw new ArgumentNullException("context");
            if (mapper == null)
                throw new ArgumentNullException("mapper");
            if (contextAtter == null)
                throw new ArgumentNullException("contextAtter");
            _contextAtter = contextAtter;
            _context = context; // new CustomerDBContext(connectionString);
            _context = context; // new CustomerDBContext(connectionString);
            _mapper = mapper;
 
            _context.Configuration.ValidateOnSaveEnabled = false;
            _context.Configuration.LazyLoadingEnabled = false;
            _context.Configuration.AutoDetectChangesEnabled = false;
            _context.Configuration.ProxyCreationEnabled = false;
        }

        public virtual IEnumerable<M> GetAll()
        {
            IEnumerable<E> entities = GetAllQuery();

            IEnumerable<M> models = new List<M>();

            foreach (var e in entities)
                models = models.Concat(new[] { ToDomainModel(e) });

            return models.ToList();
        }

        public virtual M GetById(int id)
        {
            return ToDomainModel(GetByIdQuery(id));
        }

        public virtual bool IsValueUnique(string propertyName, object value, int idToExclude = 0)
        {
            var property = _mapper.GetCorrespondingPropertyName<M, E>(propertyName);

            Expression<Func<E, bool>> finalExpression;

            // Compose the expression tree that represents the parameter to the predicate.
            var param = Expression.Parameter(typeof(E));

            // Create an expression tree that represents the expression x=>x.Name == "XYZ"
            var valueCheckExpression = Expression.Equal(
                    Expression.Property(param, property),
                    Expression.Constant(value)
              );

            //if id has been passed add condition for it to the expression tree
            if (idToExclude != 0)
            {
                //get key property of the entity
                var keyProperty = GetEntityKeyProperty<E>();

                // Create an expression tree that represents the expression x=>x.Id != 1
                var idCheckExpression = Expression.NotEqual(
                    Expression.Property(param, keyProperty.Name),
                    Expression.Constant(idToExclude)
                );

                // Create an expression tree that represents the expression (x=>x.Name == "XYZ") && (x=>x.Id != 1)
                finalExpression = Expression.Lambda<Func<E, bool>>(
                    Expression.And(valueCheckExpression, idCheckExpression),
                    param
                    );
            }
            else
            {
                finalExpression = Expression.Lambda<Func<E, bool>>(valueCheckExpression, param);
            }

            // Use the expression as required.
            bool isExists = (_context.Set<E>() as IQueryable<E>).Any(finalExpression);

            return !isExists; //if value exists in database then it is not unique            
        }

        protected virtual IEnumerable<E> GetAllQuery()
        {
            //implementation to make sure that query only returns rows where is deleted is false 
            if (typeof(E).GetProperties().Any(p => p.Name == IS_DELETED))
            {
                var param = Expression.Parameter(typeof(E));

                // Create an expression tree that represents the expression x=>x.IsDeleted == 0
                var finalExpression = Expression.Lambda<Func<E, bool>>(Expression.Equal(
                        Expression.Property(param, IS_DELETED),
                        Expression.Constant(false)
                  ), param);

                return _context.Set<E>().Where(finalExpression);
            }
            else
            {
                return _context.Set<E>();
            }
        }

        protected virtual E GetByIdQuery(int id)
        {
            return _context.Set<E>().Find(id);
        }

        protected virtual M ToDomainModel(E entity)
        {
            return _mapper.Map<E, M>(entity);
        }

        protected PropertyInfo GetEntityKeyProperty<E>()
        {
            return typeof(E).GetProperties().FirstOrDefault(p => p.Name.ToLower().Contains("id"));
        }
    }


    public abstract class BaseRepository<M, E> : BaseReadOnlyRepository<M, E>
        where M : class
        where E : class
    {
        protected readonly IUserSession _user;

        public BaseRepository(LAPPEntities context, IMapper mapper, IUserSession user, ATTEREntities contextAtter)
            : base(context, mapper, contextAtter)
        {
            if (user == null)
                throw new ArgumentNullException("User session parameter is null");
            this._user = user;
        }

        public virtual int Insert(M model)
        {
            try
            {
                E entity = ToEntityModel(model);

                _context.Set<E>().Add(entity);
                _context.Entry<E>(entity).Property(CREATED_DATE).CurrentValue = _user.DateTime;
                _context.Entry<E>(entity).Property(CREATED_BY).CurrentValue = _user.UserId;
                _context.Entry<E>(entity).Property(MODIFIED_DATE).CurrentValue = _user.DateTime;
                _context.Entry<E>(entity).Property(MODIFIED_BY).CurrentValue = _user.UserId;
                _context.Entry<E>(entity).Property(IS_DELETED).CurrentValue = false;

                int i = _context.SaveChanges();

                if (i == 0)
                    throw new Exception("Database update failed.");

                var keyProperty = base.GetEntityKeyProperty<E>();

                return (int)keyProperty.GetValue(entity, null);
            }

            catch (Exception)
            {
                throw;
            }

        }

        public virtual void Update(M model)
        {
            E entity = ToEntityModel(model);
            _context.Set<E>().Attach(entity);
            _context.Entry<E>(entity).Property(MODIFIED_DATE).CurrentValue = _user.DateTime;
            _context.Entry<E>(entity).Property(MODIFIED_BY).CurrentValue = _user.UserId;
            _context.Entry<E>(entity).State = EntityState.Modified;
            int i = _context.SaveChanges();
            if (i == 0)
                throw new Exception("Database update failed. No row to update.");
        }

        public virtual void Delete(M model)
        {
            _context.Set<E>().Remove(ToEntityModel(model));
            int i = _context.SaveChanges();
            if (i == 0)
                throw new Exception("Database delete failed. No row to delete.");
        }

        public virtual void SoftDelete(int id)
        {
            E entity = _context.Set<E>().Find(id);
            _context.Set<E>().Attach(entity);
            _context.Entry<E>(entity).Property(MODIFIED_DATE).CurrentValue = _user.DateTime;
            _context.Entry<E>(entity).Property(MODIFIED_BY).CurrentValue = _user.UserId;
            _context.Entry<E>(entity).Property(IS_DELETED).CurrentValue = true;
            _context.Entry<E>(entity).State = EntityState.Modified;

            int i = _context.SaveChanges();
            if (i == 0)
                throw new Exception("Database soft delete failed. No row to delete.");
        }

        protected virtual E ToEntityModel(M model)
        {
            return _mapper.Map<M, E>(model);
        }
    }
}
