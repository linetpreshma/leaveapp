﻿using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.Domain.IRepository;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.Repository.Dependencies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Infinite.LAPP.ATTER.EntityModel;

namespace Infinite.LAPP.Repository
{
    public class LeaveRepository : BaseRepository<LeaveRequestDomainModel, LeaveRequest>,
        ILeaveRepository
    {
        public LeaveRepository(LAPPEntities context, IMapper mapper, IUserSession user, ATTEREntities contextAtter)
            : base(context, mapper, user,contextAtter)
        {
        }
        public int ApproveRequest(ApproveLeaveDomainModel domainModel)
        {
            var entity = _context.LeaveRequests
                .Include(x => x.LeaveRequestDetails)
                .FirstOrDefault(x => x.Id == domainModel.LeaveRequestId);
            entity.Status = domainModel.Status;
            entity.ModifiedBy = _user.UserId;
            entity.ModifiedDate = _user.DateTime;
            entity.ApproverComment = domainModel.ApproverComment;
            foreach (var item in entity.LeaveRequestDetails)
            {
                var domain = domainModel.DayList.FirstOrDefault(x => x.Id == item.Id);
                if (domain.Status == 0)
                {
                    item.Status = 1;
                }
                else { item.Status = domain.Status; }
                item.Duration = (int)domain.Duration;
                _context.Entry<LeaveRequestDetail>(item).State = EntityState.Modified;
            }
            _context.Entry<LeaveRequest>(entity).State = EntityState.Modified;

            var id = _context.SaveChanges();

            return id;

        }

        public int RejectLeave(int Id, string ApproverComment)
        {
            var entity = _context.LeaveRequests
                .Include(x => x.LeaveRequestDetails)
                .FirstOrDefault(x => x.Id == Id);

            entity.Status = 2;
            entity.ApproverComment = ApproverComment;
            entity.ModifiedBy = _user.UserId;
            entity.ModifiedDate = _user.DateTime;
            foreach (var item in entity.LeaveRequestDetails)
            {
                item.Status = 2;
                _context.Entry<LeaveRequestDetail>(item).State = EntityState.Modified;
            }
            _context.Entry<LeaveRequest>(entity).State = EntityState.Modified;
            var id = _context.SaveChanges();

            return id;
        }

        public void EditLeave(EditLeaveDomainModel domainModel)
        {
            var entity = _context.LeaveRequests.FirstOrDefault(q => q.Id == domainModel.Id);
            var leaveRecordDetails = _context.LeaveRequestDetails.Where(x => x.LeaveRequestId == domainModel.Id).ToList();
            foreach (var item in leaveRecordDetails)
            {
                _context.Entry<LeaveRequestDetail>(item).State = EntityState.Deleted;
            }
            _context.Entry<LeaveRequest>(entity).State = EntityState.Deleted;
            var id = _context.SaveChanges();

            var y = _mapper.Map<EditLeaveDomainModel, LeaveRequest>(domainModel);
          
            y.Category = (int)domainModel.Category;
            y.Type = (int)domainModel.Type;

            _context.LeaveRequests.Add(y);
            var idd = _context.SaveChanges();



            //var entity = _context.LeaveRequests.FirstOrDefault(q => q.Id == domainModel.Id);

            //entity.Comments = domainModel.ApplierComment;
            //entity.Category = (int)domainModel.Category;
            //entity.ModifiedBy = _user.UserId;
            //entity.ModifiedDate = _user.DateTime;
            //var leaveRecordDetails = _context.LeaveRequestDetails.Where(x => x.LeaveRequestId == entity.Id).ToList();

            //if (domainModel.FromDate == entity.FromDate && domainModel.ToDate == entity.ToDate)
            //{
            //    foreach (var item in leaveRecordDetails)
            //    {
            //        var domainItem = domainModel.DayList.FirstOrDefault(x => x.Id == item.Id);
            //        item.Duration = (int)domainItem.Duration;
            //        item.IsDeleted = domainItem.IsDeleted;
            //        _context.Entry<LeaveRequestDetail>(item).State = EntityState.Modified;
            //    }
            //}
            //else
            //{
            //    entity.FromDate = domainModel.FromDate;
            //    entity.ToDate = domainModel.ToDate;
                
            //    foreach (var item in leaveRecordDetails)
            //    {
            //        _context.Entry<LeaveRequestDetail>(item).State = EntityState.Deleted;
            //    }

            //    var y = _mapper.Map<EditLeaveDomainModel, LeaveRequest>(domainModel);
            //    y.LeaveRequestDetails.ToList().ForEach(x => x.LeaveRequestId =entity.Id);
            //    _context.LeaveRequestDetails.AddRange(y.LeaveRequestDetails);
            //}

            //_context.Entry<LeaveRequest>(entity).State = EntityState.Modified;
            //var id = _context.SaveChanges();
        }

        public int GetReportingManagerId(int employeeId)
        {
            return _context.EmployeeDetails.FirstOrDefault(x => x.Id == employeeId).ReportingManagerId;
        }

        public void CancelLeave(int LeaveRequestId)
        {
            var entity = _context.LeaveRequestDetails.Where(x => x.LeaveRequestId == LeaveRequestId);
            foreach (var item in entity)
            {
                item.IsDeleted = true;
                _context.Entry<LeaveRequestDetail>(item).State = EntityState.Modified;
            }
            base.SoftDelete(LeaveRequestId);
            var id = _context.SaveChanges();
        }

        public void UpdateReportingManagerForLeaveRequest(int EmployeeId, int NewReportingManagerId)
        {
            var entities = _context.LeaveRequests.Where(q => q.EmployeeId == EmployeeId && (q.Status == 0 || q.Status == null));


            foreach (var item in entities)
            {
                item.ReportingMangerId = NewReportingManagerId;
                item.ModifiedBy = _user.UserId;
                item.ModifiedDate = _user.DateTime;
                _context.Entry<LeaveRequest>(item).State = EntityState.Modified;
            }


            var id = _context.SaveChanges();
        }

        public int GetEmployeeIdByLeaveId(int LeaveId)
        {
            return _context.LeaveRequests.FirstOrDefault(x => x.Id == LeaveId).EmployeeId;

        }
        public LeaveRequestDomainModel GetLeaveDetailsById(int LeaveId)
        {
            return _mapper.Map<LeaveRequest, LeaveRequestDomainModel>
            (_context.LeaveRequests.FirstOrDefault(x => x.Id == LeaveId));

        }

    }
}
