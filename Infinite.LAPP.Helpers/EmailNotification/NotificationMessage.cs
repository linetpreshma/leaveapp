﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Infinite.LAPP.Helpers.EmailNotification
{
    /// <summary>
    /// This class is used to configure email setting required to send mail.
    /// </summary>
    public class NotificationMessage
    {
        /// <summary>
        /// Constructor use to initializes a new instance of the class.
        /// </summary>
        /// <param name="recepientAddresses">The recipient addresses.</param>
        /// <param name="recepientName">Name of the recipient.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="messageBody">The message body.</param>
        /// <param name="networkCredentials">The network credential.</param>
        /// <param name="senderInfo">The sender information.</param>
        /// <returns></returns>
        public NotificationMessage(IEnumerable<string> recepientAddresses, string recepientName, string subject, string messageBody)
        {
            if (recepientAddresses == null)
                throw new ArgumentNullException("recepientAddresses", "cannot be null");

            if (string.IsNullOrEmpty(recepientName))
                throw new ArgumentNullException("recepientName", "cannot be null");

            if (string.IsNullOrEmpty(subject))
                throw new ArgumentNullException("subject", "cannot be null");

            if (string.IsNullOrEmpty(messageBody))
                throw new ArgumentNullException("messageBody", "cannot be null");

            RecepientAddresses = recepientAddresses;
            RecepientName = recepientName;
            Subject = subject;
            MessageBody = messageBody;
        }

        /// <summary>
        /// Gets or sets the recipient addresses.
        /// </summary>
        /// <value>
        /// The recipient addresses.
        /// </value>
        public IEnumerable<string> RecepientAddresses { get; set; }

        /// <summary>
        /// Gets or sets the name of the recipient.
        /// </summary>
        /// <value>
        /// The name of the recipient.
        /// </value>
        public string RecepientName { get; set; }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        /// <value>
        /// The subject.
        /// </value>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the message body.
        /// </summary>
        /// <value>
        /// The message body.
        /// </value>
        public string MessageBody { get; set; }

        public Stream Attachment { get; set; }

        public string AttachmentName { get; set; }
    }
}