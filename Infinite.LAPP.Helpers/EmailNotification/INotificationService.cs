﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infinite.LAPP.Helpers.EmailNotification
{
    public interface INotificationService
    {
        /// <summary>
        /// This method is used to send plain text notification synchronously.
        /// </summary>
        /// <param name="notificationMessage"></param>
        void SendNotification(NotificationMessage notificationMessage);

        /// <summary>
        /// This method is used to send plain text notification asynchronously.
        /// </summary>
        /// <param name="notificationMessage"></param>
        /// <returns></returns>
        Task SendNotificationAsync(NotificationMessage notificationMessage);

        /// <summary>
        /// This method is used to send templated notification synchronously.
        /// </summary>
        /// <param name="notificationMessage"></param>
        /// <param name="templateId"></param>
        /// <param name="templateTokens"></param>
        void SendTemplatedNotification(NotificationMessage notificationMessage, string templateId, Dictionary<string, string> templateTokens);
        
        /// <summary>
        /// This method is used to send templated notification asynchronously.
        /// </summary>
        /// <param name="notificationMessage"></param>
        /// <param name="templateId"></param>
        /// <param name="templateTokens"></param>
        /// <returns></returns>
        Task SendTemplatedNotificationAsync(NotificationMessage notificationMessage, string templateId, Dictionary<string, string> templateTokens);
    }
}