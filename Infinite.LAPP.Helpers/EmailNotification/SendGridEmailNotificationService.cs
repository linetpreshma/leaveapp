﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using SendGrid;
using Infinite.LAPP.Helpers.EmailNotification;
using Exceptions;

namespace Infinite.LAPP.Helpers.EmailNotifications
{
    /// <summary>
    /// A Notification class which uses SendGrid cloud service to send emails.
    /// </summary>
    public class SendGridEmailNotificationService : INotificationService
    {
        private string _sendGridAccountName;
        private string _sendGridAccountPassword;
        private string _senderEmailAddress;
        private string _senderDisplayName;

        /// <summary>
        /// Constructor used to set required values to send email.
        /// </summary>
        /// <param name="sendGridAccountName"></param>
        /// <param name="sendGridAccountPassword"></param>
        /// <param name="senderEmailAddress"></param>
        /// <param name="senderDisplayName"></param>
        public SendGridEmailNotificationService(string sendGridAccountName, string sendGridAccountPassword, string senderEmailAddress, string senderDisplayName)
        {
            _sendGridAccountName = sendGridAccountName;
            _sendGridAccountPassword = sendGridAccountPassword;
            _senderEmailAddress = senderEmailAddress;
            _senderDisplayName = senderDisplayName;
        }

        /// <summary>
        /// This method is used to send plain text email synchronously.
        /// </summary>
        /// <param name="notificationMessage"></param>
        public void SendNotification(NotificationMessage notificationMessage)
        {
            SendNotificationAsync(notificationMessage);
        }

        /// <summary>
        /// This method is used to send plain text email asynchronously.
        /// </summary>
        /// <param name="notificationMessage"></param>
        /// <returns></returns>
        public async Task SendNotificationAsync(NotificationMessage notificationMessage)
        {
            var FromMailingAddress = new MailAddress(_senderEmailAddress, _senderDisplayName);

            var sendGridMessage = new SendGridMessage()
            {
                From = FromMailingAddress,
                Subject = notificationMessage.Subject,
                Text = notificationMessage.MessageBody, // Set Text property for plain text type messages.
                Html = notificationMessage.MessageBody, // Set HTML for template type messages.
            };

            sendGridMessage.AddAttachment(notificationMessage.Attachment, notificationMessage.AttachmentName);

            // Add list of recipients to whom mail would be sent.
            sendGridMessage.AddTo(notificationMessage.RecepientAddresses);

            // Send mail using SendGrid API.
            await DeliverMailAsync(sendGridMessage);
        }

        /// <summary>
        /// This method is used to send templated email synchronously.
        /// </summary>
        /// <param name="notificationMessage"></param>
        /// <param name="templateId"></param>
        /// <param name="templateTokens"></param>
        public void SendTemplatedNotification(NotificationMessage notificationMessage, string templateId, Dictionary<string, string> templateTokens)
        {
            SendTemplatedNotificationAsync(notificationMessage, templateId, templateTokens);
        }

        /// <summary>
        /// This method is used to send templated email asynchronously.
        /// </summary>
        /// <param name="notificationMessage"></param>
        /// <param name="templateId"></param>
        /// <param name="templateTokens"></param>
        /// <returns></returns>
        public async Task SendTemplatedNotificationAsync(NotificationMessage notificationMessage, string templateId, Dictionary<string, string> templateTokens)
        {
            // Set Admin's email address to FromMailingAddress. 
            var FromMailingAddress = new MailAddress(_senderEmailAddress, _senderDisplayName);

            // Assign value from NotificationMessage to send grid message.
            var sendGridMessage = new SendGridMessage()
            {
                From = FromMailingAddress,
                Subject = notificationMessage.Subject,
                Text = notificationMessage.MessageBody, // Set Text property for plain text type messages.
                Html = notificationMessage.MessageBody, // Set HTML for template type messages.
            };

            if (notificationMessage.Attachment != null)
                sendGridMessage.AddAttachment(notificationMessage.Attachment, notificationMessage.AttachmentName);

            // Add list of recipients to whom mail would be sent.
            sendGridMessage.AddTo(notificationMessage.RecepientAddresses);

            // Add supplied token values as substitutes in SendGrid template.
            if (templateTokens.Count > 0)
            {
                foreach (var token in templateTokens) 
                {
                    //Creating a list to add token values for each recipient
                    var tokenValue = new List<string>();

                    foreach (var item in notificationMessage.RecepientAddresses)
                    {
                        tokenValue.Add(token.Value);
                    }

                    sendGridMessage.AddSubstitution(token.Key, tokenValue);
                }
            }

            // Fetch the respective template by the supplied GUID and Enable it.
            sendGridMessage.EnableTemplateEngine(templateId);

            // Supply mandatory <body> substitution tag's html. In this case its empty.
            sendGridMessage.EnableTemplate("<%body%>");

            // Send mail using SendGrid API.
            await DeliverMailAsync(sendGridMessage);
        }

        #region Private Methods

        /// <summary>
        /// Delivers the mail asynchronous using SendGrid API.
        /// </summary>
        /// <returns></returns>
        private async Task DeliverMailAsync(SendGridMessage sendGridMessage)
        {
            try
            {
                // Create an Web transport for sending email.
                var transportWeb = new Web(new NetworkCredential(_sendGridAccountName, _sendGridAccountPassword));

                // Send the email.
                await transportWeb.DeliverAsync(sendGridMessage);
            }
            // InvalidApiRequestException: custom exception thrown by SendGrid when parameters supplied to its API framework are incorrect.
            catch (InvalidApiRequestException sendGridExcptn)
            {
                throw sendGridExcptn;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
