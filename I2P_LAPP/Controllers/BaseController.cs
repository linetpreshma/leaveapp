﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Security.Claims;

namespace Infinite.LAPP.WebFront.Controllers
{
    public class BaseController : Controller
    {
        public int LoggedInUserId
        {
            get
            {
                return Convert.ToInt32(User.Identity.GetUserId());
            }
        }

        public string LoggedInRole
        {
            get
            {
                List<Claim> lstClaims = new List<Claim>(ClaimsPrincipal.Current.Claims);
                return Convert.ToString(lstClaims.Find(x => x.Type == "role").Value);
            }
        }

    }
}