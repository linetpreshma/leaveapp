﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infinite.LAPP.WebFront.Common
{
    public class ValidationMessages
    {
        public const string NameErrorMessage = "can contain only Alphabets and Special Characters like ,.-' ";
        public const string NameNumberErrorMessage = "can contain only Alphabets, Numbers and Special Characters like ,.'-";
        public const string PhoneNoErrorMessage = "must be in proper format eg. XXXXXXXXXX";
        public const string RequiredValidationMessage = "is required";
        public const string UniqueValidationMessage = "already exists";
        public const string LengthLimit = "should not be over {1} characters";
        public const string LengthLimitForNumbers = "should be between {1} and {2}";
        public const string IncorrectFormat = "incorrect format";
        public const string InvalidEmailPassword = "Invalid email or password. Please try again";
        public const string AccountLock = "Your account is locked. Please try again later. ";
        public const string MinimumLength = "must be at least {2} characters";
        public const string DoesNotMatch = "does not match.";
        public const string Invalid = "is invalid";
        public const string NegativeValues = "cannot be negative";
        public const string AlphaNumericeErrorMessage = "can contain only alpha numeric values and Special Character -";
        public const string PasswordFormatErrorMessage = "must contain alphabets and numbers";
        public const string PasswordLength = "must be {2} to {1} characters long";
        public const string NewPassword = "must not be same as current password";
        public const string Number = "must be a number";
        public const string EmployeeCodeErrorMessage = "Must be an integer of 4 digits";
    }

    public class DisplayFormatConstants
    {
        public const string DateFormat = "dd/MMM/yyyy";
        public const string DateFormatString = "{0:" + DateFormat + "}";
        public const string CurrencyFormatString = "{0:c}";
        public const string JqueryDatePickerFormat = "dd/MM/yy";
        public const string CurrentCulture = "en-US";
        public const string DateTimePickerFormat = "m/d/Y H:i";
        public const string DateTimePickerFormatString = "{0:" + DateTimePickerFormat + "}";
        public const string DateTime24HourFormat = "dd/MM/yyyy HH:mm";
    }

    public class ValidationRegex
    {
        public const string NameFormat = "^[a-zA-Z ,.'-]+$";
        public const string NameFormatWithNumbers = "^[ A-Za-z0-9 ,.'-]+$";
        public const string PhoneNo = "^[0-9]{10}$";
        public const string Currency = @"^-?\d+(\.\d{1,10})?$";
        public const string IntegersOnly = @"^[0-9]*$";
        public const string PasswordRegex = @"^(?=.*[a-zA-Z])(?=.*\d).{1,100}$";
        public const string YearFormat = "^[0-9]{4,4}$";
        public const string EmployeeCodeFormat = "^[0-9]{4,4}$";
    }
}
