﻿using Infinite.LAPP.WebFront.MVCInfrastructure.DIContainer;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infinite.LAPP.WebFront.Models
{
    public class ClaimsModel
    {
        private IAuthenticationManager _authenticationManager;

        public ClaimsModel()
        {
            _authenticationManager = (IAuthenticationManager)CastleWindsorContainer.Container.Resolve(typeof(IAuthenticationManager));
        }

        public bool CanViewRole
        {
            get
            {
                return _authenticationManager.User.HasClaim("Role Management", "canViewRole");
            }
        }

        public bool CanCreateNewRole
        {
            get
            {
                return _authenticationManager.User.HasClaim("Role Management", "canAddRole");
            }
        }

        public bool CanEditRole
        {
            get
            {
                return _authenticationManager.User.HasClaim("Role Management", "canEditRole");
            }
        }

        public bool CanViewUser
        {
            get
            {
                return _authenticationManager.User.HasClaim("User Management", "canViewUser");
            }
        }

        public bool CanCreateUser
        {
            get
            {
                return _authenticationManager.User.HasClaim("User Management", "canAddUser");
            }
        }

        public bool CanEditUser
        {
            get
            {
                return _authenticationManager.User.HasClaim("User Management", "canEditUser");
            }
        }

        public bool CanApproveRejectLeave
        {
            get
            {
                return _authenticationManager.User.HasClaim("Leave Management", "canApproveLeave");
            }
        }

        public bool CanViewHoliday
        {
            get
            {
                return _authenticationManager.User.HasClaim("Holiday Management", "canViewHoliday");
            }
        }

        public bool CanAddHoliday
        {
            get
            {
                return _authenticationManager.User.HasClaim("Holiday Management", "canAddHoliday");
            }
        }

        public bool CanEditHoliday
        {
            get
            {
                return _authenticationManager.User.HasClaim("Holiday Management", "canEditHoliday");
            }
        }


    }
}