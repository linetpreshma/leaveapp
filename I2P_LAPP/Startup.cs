﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Infinite.LAPP.WebFront.Startup))]
namespace Infinite.LAPP.WebFront
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
