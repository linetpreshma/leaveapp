﻿using Infinite.LAPP.Repository.Dependencies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;

namespace Infinite.LAPP.WebFront.MVCInfrastructure
{
    public class CustomUserSession : IUserSession
    {
        public int UserId
        {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.User == null)
                    return 0;
                return Convert.ToInt32(HttpContext.Current.User.Identity.GetUserId());
            }
        }

        public DateTime DateTime
        {
            get
            {
                return DateTime.Now;
            }
        }
    }
}