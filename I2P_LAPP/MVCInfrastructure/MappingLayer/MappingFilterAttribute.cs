﻿using AutoMapper;
using System;
using System.Web.Mvc;

namespace  Infinite.LAPP.WebFront.MVCInfratructure.MappingLayer
{
    [AttributeUsage(AttributeTargets.Method)]
    public class MappingFilterAttribute : ActionFilterAttribute
    {
        private readonly Type _source;
        private readonly Type _destination;

        public MappingFilterAttribute(Type source, Type destination)
        {
            _source = source;
            _destination = destination;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var model = filterContext.Controller.ViewData.Model;

            var viewModel = Mapper.Map(model, _source, _destination);
            
            filterContext.Controller.ViewData.Model = viewModel;
        }

    }
}