﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Castle.Core.Internal;
using Infinite.LAPP.Repository.Dependencies;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.WebFront.Areas.Leave.Models;
using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.WebFront.MVCInfrastructure.IdentityFramework;
using Infinite.LAPP.WebFront.Areas.Users.Models;
using Infinite.LAPP.WebFront.Areas.Settings.Models;
using Infinite.LAPP.WebFront.Areas.Employee.Models;


namespace Infinite.LAPP.WebFront.MVCInfrastructure
{
    public class AutoMapperRegistry
    {
        IUserSession _session;


        public AutoMapperRegistry(IUserSession session)
        {
            if (session == null)
                throw new ArgumentNullException("session parameter is null");

            _session = session;
        }

        public void Register()
        {
            Mapper.Initialize(cfg =>
            {

                #region User Role Module
                cfg.CreateMap<RegisterViewModel, User>()
                    .ForMember(d => d.EmailAddress, y => y.MapFrom(s => s.Email))
                    .ForMember(d => d.Role, y => y.Ignore())
                    .ForMember(d => d.RoleId, y => y.Ignore())
                    .ForMember(d => d.UserId, y => y.Ignore())
                    .ForMember(d => d.IsActive, y => y.Ignore());
                cfg.CreateMap<AspNetRole, Permission>()
                    .ForMember(dest => dest.RoleId, y => y.MapFrom(src => src.Id))
                    .ForMember(dest => dest.RoleName, y => y.MapFrom(src => src.Name))
                    .ForMember(dest => dest.RoleDisplayName, y => y.MapFrom(src => src.DisplayName))
                    .ForMember(dest => dest.RoleDescription, y => y.MapFrom(src => src.Description))
                    .ForMember(dest => dest.Modules, y => y.MapFrom(src => src.AspNetCustomRolePermissions));
                cfg.CreateMap<Permission, AspNetRole>()
                   .ForMember(dest => dest.Name, y => y.MapFrom(src => src.RoleName))
                   .ForMember(dest => dest.DisplayName, y => y.MapFrom(src => src.RoleDisplayName))
                   .ForMember(dest => dest.Description, y => y.MapFrom(src => src.RoleDescription))
                   .ForMember(dest => dest.AspNetCustomRolePermissions, y => y.MapFrom(src => src.Modules))
                   .ForMember(dest => dest.CreatedBy, y => y.MapFrom(x => _session.UserId))
                   .ForMember(dest => dest.ModifiedBy, y => y.MapFrom(x => _session.UserId))
                   .ForMember(dest => dest.CreatedDate, y => y.MapFrom(x => _session.DateTime))
                   .ForMember(dest => dest.ModifiedDate, y => y.MapFrom(x => _session.DateTime))
                   .ForMember(dest => dest.Id, y => y.Ignore())
                   .ForMember(dest => dest.IsDeleted, y => y.Ignore())
                   .ForMember(dest => dest.AspNetUserRoles, y => y.Ignore())
                   .ForMember(dest => dest.EmployeeDetails, y => y.Ignore())
                   .ForMember(dest => dest.IsTrainee, y => y.MapFrom(s =>s.IsTrainee))
                   .ForMember(dest => dest.IsReportingManager, y => y.MapFrom(s => s.IsReportingManager))
                   .AfterMap(MapPermissionToRoleEntity);
                cfg.CreateMap<User, AspNetUser>()
                    .AfterMap(CreateUserRoleEntity)
                    .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.EmailAddress))
                    .ForMember(dest => dest.PasswordHash, opt => opt.MapFrom(src => src.Password))
                    .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.EmailAddress))
                    .ForMember(dest => dest.EmailConfirmed, opt => opt.UseValue(true))
                    .ForMember(dest => dest.PhoneNumberConfirmed, opt => opt.UseValue(true))
                    .ForMember(dest => dest.LockoutEnabled, opt => opt.UseValue(true))
                    .ForMember(dest => dest.AccessFailedCount, opt => opt.UseValue(0))
                    .ForMember(dest => dest.IsDeleted, opt => opt.UseValue(false))
                    .ForMember(dest => dest.SecurityStamp, opt => opt.UseValue(Guid.NewGuid().ToString()))
                    .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(x => _session.UserId))
                    .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(x => _session.DateTime))
                    .ForMember(dest => dest.ModifiedBy, opt => opt.MapFrom(x => _session.UserId))
                    .ForMember(dest => dest.ModifiedDate, opt => opt.MapFrom(x => _session.DateTime))
                    .ForMember(dest => dest.Id, opt => opt.Ignore())
                    .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                    .ForMember(dest => dest.LockoutEndDateUtc, opt => opt.Ignore())
                    .ForMember(dest => dest.AspNetUserClaims, opt => opt.Ignore())
                    .ForMember(dest => dest.AspNetUserLogins, opt => opt.Ignore())
                    .ForMember(dest => dest.EmployeeUserMappingTables, opt => opt.Ignore())
                    .ForMember(dest => dest.AspNetUserRoles, opt => opt.Ignore());

                cfg.CreateMap<AspNetUser, AspNetUserDomainModel>();
                cfg.CreateMap<List<AspNetUser>, List<User>>();
                cfg.CreateMap<AspNetUser, User>()
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(s => s.Id))
                    .ForMember(dest => dest.EmailAddress, opt => opt.MapFrom(s => s.Email))
                    .ForMember(dest => dest.Password, opt => opt.MapFrom(s => s.PasswordHash))
                    .ForMember(dest => dest.Role, opt => opt.Ignore())
                    .ForMember(dest => dest.RoleId, opt => opt.MapFrom(s => s.AspNetUserRoles.FirstOrDefault(x => x.UserId == s.Id).RoleId))
                    .AfterMap(MapRoleToUser);
                cfg.CreateMap<vw_RoleUserCount, Role>();
                cfg.CreateMap<Module, AspNetCustomRolePermission>()
                    .ForMember(dest => dest.AccessLevel, y => y.MapFrom(src => src.AccessRight))
                    .ForMember(dest => dest.ModuleId, y => y.MapFrom(src => src.ModuleId))
                    .ForMember(dest => dest.CreatedBy, y => y.MapFrom(x => _session.UserId))
                    .ForMember(dest => dest.ModifiedBy, y => y.MapFrom(x => _session.UserId))
                    .ForMember(dest => dest.CreatedDate, y => y.MapFrom(x => _session.DateTime))
                    .ForMember(dest => dest.ModifiedDate, y => y.MapFrom(x => _session.DateTime))
                    .ForMember(dest => dest.Id, y => y.Ignore())
                    .ForMember(dest => dest.RoleId, y => y.Ignore())
                    .ForMember(dest => dest.IsDeleted, y => y.Ignore())
                    .ForMember(dest => dest.AspNetRole, y => y.Ignore())
                    .ForMember(dest => dest.AspNetCustomModule, y => y.Ignore());
                cfg.CreateMap<AspNetCustomRolePermission, Domain.DomainModel.Module>()
                    .ForMember(dest => dest.ModuleId, y => y.MapFrom(src => src.ModuleId))
                    .ForMember(dest => dest.ModuleName,
                        y => y.MapFrom(src => src.AspNetCustomModule != null ? src.AspNetCustomModule.Name : string.Empty))
                    .ForMember(dest => dest.AccessRight, y => y.MapFrom(src => src.AccessLevel))
                    .ForMember(dest => dest.Claims,
                               y => y.MapFrom(src => src.AspNetCustomModule != null && src.AspNetCustomModule.AspNetCustomClaims != null
                                                 ? src.AspNetCustomModule.AspNetCustomClaims.Select(c => c.Claim).ToList()
                                                 : new List<string>()));
                cfg.CreateMap<vw_RoleUserCount, RoleViewModel>();
                cfg.CreateMap<List<vw_RoleUserCount>, RoleManagerViewModel>().AfterMap(ReturnRoleManagerViewModelTwo).ForAllMembers(x => x.Ignore());
                cfg.CreateMap<Role, RoleViewModel>();
                cfg.CreateMap<List<Role>, RoleManagerViewModel>().AfterMap(ReturnRoleManagerViewModel).ForAllMembers(x => x.Ignore());
                cfg.CreateMap<Permission, EditRoleViewModel>()
                   .ForMember(dest => dest.Name, y => y.MapFrom(src => src.RoleName))
                   .ForMember(dest => dest.RoleId, y => y.MapFrom(src => src.RoleId))
                   .ForMember(dest => dest.DisplayName, y => y.MapFrom(src => src.RoleDisplayName))
                   .ForMember(dest => dest.Description, y => y.MapFrom(src => src.RoleDescription))
                   .ForMember(dest => dest.IsPreDefinedRole, y => y.MapFrom(src => src.IsTrainee))
                   .ForMember(dest => dest.RolePermissionList, y => y.MapFrom(src => src.Modules))
                   .AfterMap(ReturnEditRoleViewModel);
                cfg.CreateMap<List<Module>, CreateRoleViewModel>()
                    .AfterMap(ReturnCreateRoleViewModel)
                    .ForAllMembers(x => x.Ignore());
                cfg.CreateMap<Module, RolePermissionViewModel>()
                    .ForMember(dest => dest.AccessRights, y => y.MapFrom(src => src.AccessRight))
                    .ForMember(dest => dest.ModuleId, y => y.MapFrom(src => src.ModuleId))
                    .ForMember(dest => dest.ModuleName, y => y.MapFrom(src => src.ModuleName))
                    .ForMember(dest => dest.RoleId, y => y.Ignore())
                    .ForMember(dest => dest.Id, y => y.Ignore());
                cfg.CreateMap<User, CreateUserViewModel>()
                    .ForMember(d => d.Password, y => y.Ignore())
                    .ForMember(d => d.ConfirmPassword, y => y.Ignore());
                //edit user view model mapping
                cfg.CreateMap<User, EditUserViewModel>()
                    .ForMember(dest => dest.FirstName, y => y.MapFrom(src => src.FirstName))
                    .ForMember(dest => dest.LastName, y => y.MapFrom(src => src.LastName))
                    .ForMember(dest => dest.UserId, y => y.MapFrom(src => src.UserId))
                    .ForMember(dest => dest.EmailAddress, y => y.MapFrom(src => src.EmailAddress))
                    .ForMember(dest => dest.RoleId, y => y.MapFrom(src => src.RoleId))
                    .ForMember(dest => dest.PhoneNumber, y => y.MapFrom(src => src.PhoneNumber))
                    .ForMember(dest => dest.IsActive, y => y.MapFrom(src => src.IsActive))
                    .ForMember(dest => dest.IsActiveOriginal, y => y.MapFrom(src => src.IsActive));
                //edit user profile view model mapping
                cfg.CreateMap<User, EditUserProfileViewModel>()
                    .ForMember(dest => dest.FirstName, y => y.MapFrom(src => src.FirstName))
                    .ForMember(dest => dest.LastName, y => y.MapFrom(src => src.LastName))
                    .ForMember(dest => dest.EmailAddress, y => y.MapFrom(src => src.EmailAddress))
                    .ForMember(dest => dest.PhoneNumber, y => y.MapFrom(src => src.PhoneNumber));
                //display users view model mapping
                cfg.CreateMap<User, DisplayUsersViewModel>()
                    .ForMember(dest => dest.UserId, y => y.MapFrom(src => src.UserId))
                    .ForMember(dest => dest.FirstName, y => y.MapFrom(src => src.FirstName))
                    .ForMember(dest => dest.LastName, y => y.MapFrom(src => src.LastName))
                    .ForMember(dest => dest.Role, y => y.MapFrom(src => src.Role))
                    .ForMember(dest => dest.RoleId, y => y.MapFrom(src => src.RoleId));
                #endregion

                #region Holiday
                cfg.CreateMap<Holiday, HolidaysViewModel>()
                    .ForMember(d => d.Claim, y => y.Ignore());
                cfg.CreateMap<Holiday, HolidaysDomainModel>();
                cfg.CreateMap<HolidaysDomainModel, HolidaysViewModel>()
                   .ForMember(d => d.Claim, y => y.Ignore());
                cfg.CreateMap<HolidaysViewModel, HolidaysDomainModel>();
                cfg.CreateMap<HolidaysDomainModel, Holiday>()
                    .ForMember(d => d.CreatedBy, y => y.UseValue(_session.UserId))
                    .ForMember(d => d.CreatedDate, y => y.UseValue(_session.DateTime))
                    .ForMember(d => d.ModifiedBy, y => y.UseValue(_session.UserId))
                    .ForMember(d => d.ModifiedDate, y => y.UseValue(_session.DateTime))
                    .ForMember(d => d.IsDeleted, y => y.UseValue(false));
                #endregion

                #region Leaves
                cfg.CreateMap<LeaveRequest, ManageLeavesViewModel>()
                    .ForMember(d => d.ApplierComment, y => y.MapFrom(s => s.Comments))
                    .ForMember(d => d.EmployeeCode, y => y.Ignore())
                    .ForMember(d => d.ApproverName, y => y.Ignore())
                    .ForMember(d => d.ReportingManagerId, y => y.MapFrom(s => s.ReportingMangerId))
                    .ForMember(d => d.DayList, y => y.Ignore())
                    .ForMember(d => d.EmployeeCode, y => y.MapFrom(s => s.EmployeeDetail.EmployeeCode))
                    .ForMember(d => d.EmployeeName, y => y.MapFrom(s => s.EmployeeDetail.FirstName + " " + s.EmployeeDetail.LastName))
                    .AfterMap(MapLeaveRequestDetails);
                cfg.CreateMap<LeaveRequestDetail, Dates>();
                cfg.CreateMap<vw_GetAllLeavesListwithDetails, LeaveViewModel>();
                cfg.CreateMap<CreateLeaveRequestViewModel, LeaveRequestDomainModel>()
                    .ForMember(d => d.Status, y => y.UseValue(0))
                    .ForMember(d => d.ReportingManagerId, y => y.Ignore())
                    .ForMember(d => d.DayList, y => y.Ignore())
                    .AfterMap(MapDatesDomainModel);
                cfg.CreateMap<Dates, DatesDomainModel>();
                cfg.CreateMap<LeaveRequest, LeaveRequestDomainModel>()
                 .ForMember(d => d.LeaveType, y => y.Ignore())
                 .ForMember(d => d.DayList, y => y.Ignore())
                 .ForMember(d => d.ApplierComment, y => y.Ignore())
                 .ForMember(d => d.ReportingManagerId, y => y.Ignore());
                cfg.CreateMap<LeaveRequestDomainModel, LeaveRequest>()
                    .ForMember(d => d.LeaveRequestDetails, y => y.Ignore())
                    .ForMember(d => d.CreatedBy, y => y.Ignore())
                    .ForMember(d => d.CreatedDate, y => y.Ignore())
                    .ForMember(d => d.ModifiedBy, y => y.Ignore())
                    .ForMember(d => d.ModifiedDate, y => y.Ignore())
                    .ForMember(d => d.Type, y => y.MapFrom(s => s.LeaveType))
                    .ForMember(d => d.Id, y => y.MapFrom(s => s.ID))
                    .ForMember(d => d.IsDeleted, y => y.Ignore())
                    .ForMember(d => d.EmployeeDetail, y => y.Ignore())
                    .ForMember(d => d.ApproverComment, y => y.Ignore())
                    .ForMember(d => d.Status, y => y.MapFrom(s => s.Status))
                    .ForMember(d => d.Comments, y => y.MapFrom(s => s.ApplierComment))
                    .ForMember(d => d.ReportingMangerId, y => y.MapFrom(s => s.ReportingManagerId))
                    .AfterMap(MapLeaveRequestDetails);
                cfg.CreateMap<DatesDomainModel, LeaveRequestDetail>()
                    .ForMember(d => d.LeaveRequest, y => y.Ignore())
                    .ForMember(d => d.LeaveRequestId, y => y.Ignore());
                cfg.CreateMap<vw_GetAllLeavesList, DisplayLeaveRequestDetailsModel>()
                   .ForMember(d => d.LeaveType, y => y.MapFrom(s => s.Type))
                   .ForMember(d => d.ReportingManagerName, y => y.MapFrom(s => s.ReportingManagerName))
                   .ForMember(d => d.Status, y => y.MapFrom(s => s.LeaveRequestStatus));

                cfg.CreateMap<LeaveRequest, ApproveLeaveRequestDetailsModel>()
                  .ForMember(d => d.LeaveType, y => y.MapFrom(s => s.Type))
                  .ForMember(d => d.EmployeeCode, y => y.MapFrom(s => s.EmployeeDetail.EmployeeCode))
                  .ForMember(d => d.EmployeeName, y => y.MapFrom(s => s.EmployeeDetail.FirstName + " " + s.EmployeeDetail.LastName))
                  .ForMember(d => d.CreatedDate, y => y.MapFrom(s => s.CreatedDate))
                  .ForMember(d => d.CategoryName, y => y.Ignore())
                  .ForMember(d => d.Status, y => y.MapFrom(s => s.Status));
                cfg.CreateMap<vw_GetAllLeavesListForApproval, ApproveLeaveRequestDetailsModel>()
                  .ForMember(d => d.LeaveType, y => y.MapFrom(s => s.Type))
                  .ForMember(d => d.EmployeeCode, y => y.MapFrom(s => s.EmployeeCode))
                  .ForMember(d => d.EmployeeName, y => y.MapFrom(s => s.EmployeeName))
                  .ForMember(d => d.CreatedDate, y => y.MapFrom(s => s.CreatedDate))
                  .ForMember(d => d.Status, y => y.MapFrom(s => s.LeaveRequestStatus));

                cfg.CreateMap<vw_GetAllLeavesListwithDetails, LeaveViewModel>();

                //CreateMap<LeaveRequest, EditLeaveViewModel>
                cfg.CreateMap<EditLeaveViewModel, EditLeaveDomainModel>();
                cfg.CreateMap<EditLeaveDomainModel, LeaveRequest>()
                    .ForMember(d => d.LeaveRequestDetails, y => y.Ignore())
                    .ForMember(d => d.CreatedBy, y => y.UseValue(_session.UserId))
                    .ForMember(d => d.CreatedDate, y => y.UseValue(_session.DateTime))
                    .ForMember(d => d.ModifiedBy, y => y.UseValue(_session.UserId))
                    .ForMember(d => d.ModifiedDate, y => y.UseValue(_session.DateTime))
                    .ForMember(d => d.Comments, y => y.MapFrom(s=>s.ApplierComment))
                    .ForMember(d => d.IsDeleted, y => y.Ignore())
                    .ForMember(d => d.ReportingMangerId, y => y.MapFrom(s => s.ReportingManagerId))
                    .ForMember(d => d.EmployeeDetail, y => y.Ignore())
                    .AfterMap(MapLeaveRequestDetails);


                cfg.CreateMap<LeaveRequest, EditLeaveViewModel>()
                .ForMember(d => d.ApplierComment, y => y.MapFrom(s => s.Comments))
                .ForMember(d => d.ApproverComment, y => y.MapFrom(s => s.ApproverComment))
                .ForMember(d => d.EmployeeCode, y => y.Ignore())
                .ForMember(d => d.ApproverName, y => y.Ignore())
                .ForMember(d => d.ReportingManagerId, y => y.MapFrom(s => s.ReportingMangerId))
                .ForMember(d => d.DayList, y => y.Ignore())
                .ForMember(d => d.EmployeeCode, y => y.MapFrom(s => s.EmployeeDetail.EmployeeCode))
                .ForMember(d => d.EmployeeName, y => y.MapFrom(s => s.EmployeeDetail.FirstName + " " + s.EmployeeDetail.LastName))
                .AfterMap(MapLeaveRequestDetails);
          
                
                #endregion

                #region Employee
                //convert Employee to  Employee domain
                cfg.CreateMap<EmployeeDetail, EmployeeDomainModel>()
                    .ForMember(d => d.RoleId, y => y.MapFrom(s => s.RoleId));
                cfg.CreateMap<EmployeeDomainModel, EmployeeViewModel>()
                    .ForMember(d => d.EmailAddress, y => y.Ignore())
                    .ForMember(d => d.LastName, y => y.Ignore())
                    .ForMember(d => d.FirstName, y => y.MapFrom(s => s.FirstName))
                    .ForMember(d => d.Role, y => y.Ignore());
                cfg.CreateMap<EmployeeViewModel, EmployeeDomainModel>();
                cfg.CreateMap<EmployeeDomainModel, EmployeeDetail>()
                    .ForMember(d => d.RoleId, y => y.MapFrom(s => s.RoleId))
                    .ForMember(d => d.DashboardDetails, y => y.Ignore())
                    .ForMember(d => d.ModifiedBy, y => y.Ignore())
                    .ForMember(d => d.ModifiedDate, y => y.Ignore())
                    .ForMember(d => d.EmployeeUserMappingTables, y => y.Ignore())
                    .ForMember(d => d.AspNetRole, y => y.Ignore())
                    .ForMember(d => d.LeaveRequests, y => y.Ignore())
                    .ForMember(d => d.IsDeleted, y => y.Ignore());
                cfg.CreateMap<vw_GetEmployeeList, DisplayEmployeeListViewModel>()
                    .ForMember(d => d.Location, y => y.MapFrom(s => s.Location));

                cfg.CreateMap<vw_GetEmployeeList, EmployeeViewModel>()
                    .ForMember(d => d.Role, y => y.Ignore())
                    .ForMember(d => d.IsDeactiavted, y => y.Ignore())
                   .ForMember(d => d.EmailAddress, y => y.MapFrom(s => s.Email));

                cfg.CreateMap<AddEmployeeRoleDomainModel, AddEmployeeRoleModel>();
                #endregion

                cfg.CreateMap<ApplicationSetting, ApplicationSettingsModel>();

                cfg.CreateMap<vw_GetEmployeeList, vw_GetEmployeeListDomainModel>();

                cfg.CreateMap<DashboardDetail, DashboardDetailViewModel>()
                    .ForMember(d => d.EmployeeDetail, y => y.Ignore());

                cfg.CreateMap<DashboardDetailDomainModel, DashboardDetail>()
                    .ForMember(d => d.EmployeeDetail, y => y.Ignore());


                cfg.CreateMap<LeaveConstant, LeaveConstantDomainModel>();


            });

            Mapper.AssertConfigurationIsValid();
        }

        private void MapLeaveRequestDetails(EditLeaveDomainModel domainModel, LeaveRequest entityModel, ResolutionContext context)
        {
            entityModel.LeaveRequestDetails = Mapper.Map<List<DatesDomainModel>, List<LeaveRequestDetail>>(domainModel.DayList);
        }

        private void MapLeaveRequestDetails(LeaveRequest entity, EditLeaveViewModel viewModel, ResolutionContext context)
        {
            viewModel.DayList = context.Mapper.Map<List<LeaveRequestDetail>, List<Dates>>(entity.LeaveRequestDetails.ToList());
        }

        private void MapLeaveRequestDetails(LeaveRequest entity, ManageLeavesViewModel viewModel, ResolutionContext context)
        {
            viewModel.DayList = context.Mapper.Map<List<LeaveRequestDetail>, List<Dates>>(entity.LeaveRequestDetails.ToList());
        }



        private void MapLeaveRequestDetails(LeaveRequestDomainModel domainModel, LeaveRequest entityModel, ResolutionContext context)
        {
            entityModel.LeaveRequestDetails = Mapper.Map<List<DatesDomainModel>, List<LeaveRequestDetail>>(domainModel.DayList);
        }

        private void MapDatesDomainModel(CreateLeaveRequestViewModel viewModel, LeaveRequestDomainModel domainModel, ResolutionContext context)
        {
            domainModel.DayList = new List<DatesDomainModel>();
            foreach (var item in viewModel.DayList)
            {
                if (item.Status != 2)
                {
                    domainModel.DayList.Add(Mapper.Map<Dates, DatesDomainModel>(item));
                }
            }
            
            //domainModel.DayList = Mapper.Map<List<Dates>, List<DatesDomainModel>>(viewModel.DayList);
        }

        private void MapRoleToUser(AspNetUser user, User domainUser, ResolutionContext context)
        {
            domainUser.Role = user.AspNetUserRoles.FirstOrDefault(x => x.UserId == domainUser.UserId).AspNetRole.DisplayName;
        }

        private void CreateUserRoleEntity(User user, AspNetUser aspNetUser)
        {
            aspNetUser.AspNetUserRoles = new List<AspNetUserRole>() 
            { 
                new AspNetUserRole() 
                { 
                        CreatedBy = _session.UserId,
                        CreatedDate= _session.DateTime,
                        ModifiedBy= _session.UserId,
                        ModifiedDate = _session.DateTime,
                        RoleId = user.RoleId,
                        UserId= user.UserId
                } 
            };
        }

        private void MapPermissionToRoleEntity(Permission permission, AspNetRole aspNetRole)
        {
            if (aspNetRole.AspNetCustomRolePermissions != null && aspNetRole.AspNetCustomRolePermissions.Any())
            {
                aspNetRole.AspNetCustomRolePermissions.ForEach(x => x.RoleId = aspNetRole.Id);
            }
        }

        private void ReturnEditRoleViewModel(Permission permission, EditRoleViewModel editRoleViewModel)
        {
            if (editRoleViewModel.RolePermissionList != null && editRoleViewModel.RolePermissionList.Any())
            {
                editRoleViewModel.RolePermissionList.ForEach(x => x.RoleId = permission.RoleId);
            }
        }

        private void ReturnRoleManagerViewModel(List<Role> roles, RoleManagerViewModel roleManagerViewModel)
        {
            roleManagerViewModel.RoleViewModelList = Mapper.Map<List<Role>, List<RoleViewModel>>(roles);
        }

        private void ReturnRoleManagerViewModelTwo(List<vw_RoleUserCount> roles, RoleManagerViewModel roleManagerViewModel)
        {
            roleManagerViewModel.RoleViewModelList = Mapper.Map<List<vw_RoleUserCount>, List<RoleViewModel>>(roles);
        }

        private void ReturnCreateRoleViewModel(List<Module> moduleList, CreateRoleViewModel userRoleViewModel)
        {
            if (userRoleViewModel.RolePermissionList == null)
            {
                userRoleViewModel.RolePermissionList = new List<RolePermissionViewModel>();
            }

            foreach (var module in moduleList)
            {
                var rolePermissionModule = new RolePermissionViewModel
                {
                    ModuleId = module.ModuleId,
                    ModuleName = module.ModuleName,
                    AccessRights = "Denied"
                };

                userRoleViewModel.RolePermissionList.Add(rolePermissionModule);
            }
        }

    }
}