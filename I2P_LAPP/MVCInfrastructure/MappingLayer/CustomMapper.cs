﻿using AutoMapper;
using mapper = Infinite.LAPP.Repository.Dependencies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infinite.LAPP.WebFront.MVCInfratructure.MappingLayer
{
    public class CustomMapper : mapper.IMapper
    {
        public T2 Map<T1, T2>(T1 obj)
        {
            return Mapper.Map<T1, T2>(obj);
        }

        public string GetCorrespondingPropertyName<T1, T2>(string sourcePropertyName)
        {
            TypeMap typeMap = Mapper.Configuration.FindTypeMapFor<T1, T2>();
            return typeMap
                .GetPropertyMaps()
                .Where(p => p.SourceMember.Name == sourcePropertyName)
                .FirstOrDefault()
                .DestinationProperty
                .Name;
            
        }


        public List<T2> Map<T1, T2>(List<T1> obj)
        {
            var objList = new List<T2>();

            //checking whether mapping config exists or not.
            var mappingObject = Mapper.Configuration.FindTypeMapFor<T1, T2>();

            if (mappingObject == null)
            {
                //if mapping config is not exists then creating new mapping config.
                Mapper.Initialize(x=>x.CreateMap<T1, T2>());
            }

            if (obj != null && obj.Any())
            {
                objList = Mapper.Map<List<T1>, List<T2>>(obj);
            }

            return objList;
        }
    }
}