﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using System;
using System.Web;
using System.Web.Mvc;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.WebFront.Areas.Users.Controllers;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Infinite.LAPP.WebFront.MVCInfrastructure.IdentityFramework;
using Infinite.LAPP.Domain.IRepository;
using Infinite.LAPP.Domain.Service;
using Infinite.LAPP.Repository;
using Infinite.LAPP.WebFront.Areas.Leave.Controllers;
using Infinite.LAPP.Repository.QueryObjects;
using Infinite.LAPP.WebFront.MVCInfratructure.MappingLayer;
using Infinite.LAPP.Repository.Dependencies;
using Infinite.LAPP.WebFront.Areas.Settings.Controllers;
using Infinite.LAPP.WebFront.Areas.Employee.Controllers;
using Infinite.LAPP.Helpers.EmailNotifications;
using Infinite.LAPP.ATTER.EntityModel;

namespace Infinite.LAPP.WebFront.MVCInfrastructure.DIContainer
{
    internal class CastleWindsorContainer : IContainer
    {
        public static WindsorContainer Container { get; private set; }

        public CastleWindsorContainer()
        {
            Container = new WindsorContainer();
        }

        public void Register()
        {
            //Register custom action invoker. Not currently required.
            //Container.Register(Component.For<IActionInvoker>().ImplementedBy<WindsorActionInvoker>().LifestyleTransient());

           // register components required by identity framework
            Container.Register(
                //authentication maanger: owin context 
                Component.For<IAuthenticationManager>().UsingFactoryMethod(c => HttpContext.Current.GetOwinContext().Authentication).LifestyleTransient(),
                //user store registration
                Component.For<IUserStore<CustomIdentityUser, int>>().ImplementedBy<CustomIdentityUserStore>().LifestyleTransient(),
                //role store registration
                Component.For<IRoleStore<CustomIdentityRole, int>>().ImplementedBy<CustomIdentityRoleStore>().LifestyleTransient()
                );

            Container.Register(
                Component.For<IPermissionRepository>().ImplementedBy<PermissionRepository>().LifestyleTransient(),
                Component.For<IUserRolesRepository>().ImplementedBy<UserRolesRepository>().LifestyleTransient(),
                Component.For<IRoleRepository>().ImplementedBy<RoleRepository>().LifestyleTransient(),
                Component.For<IModuleRepository>().ImplementedBy<ModuleRepository>().LifestyleTransient(),
                Component.For<IRolePermissionRepository>().ImplementedBy<RolePermissionRepository>().LifestyleTransient(),
                Component.For<IUserRepository>().ImplementedBy<UserRepository>().LifestyleTransient(),
                Component.For<IHolidaysRepository>().ImplementedBy<HolidaysRepository>().LifestyleTransient(),
                Component.For<ILeaveRepository>().ImplementedBy<LeaveRepository>().LifestyleTransient(),
                Component.For<ISettingsRepository>().ImplementedBy<SettingsRepository>().LifestyleTransient(),
                Component.For<IEmployeeRepository>().ImplementedBy<EmployeeRepository>().LifestyleTransient()
                );
            ////register dependencies for repositories
            Container.Register(
                Component.For<IUserSession>().ImplementedBy<CustomUserSession>().LifestyleTransient(),
                Component.For<IMapper>().ImplementedBy<CustomMapper>().LifestyleTransient()
            //    Component.For<ICacheProvider>().ImplementedBy<DefaultCacheProvider>().LifestyleTransient()
                );

            //register controllers, R&D: register by namespace
            Container.Register(
                  Component.For<WebAccountController>().LifestyleTransient(),
                  Component.For<DashboardController>().LifestyleTransient(),
                  Component.For<RoleManagerController>().LifestyleTransient(),
                  Component.For<ManageLeavesController>().LifestyleTransient(),
                  Component.For<UserController>().LifestyleTransient(),
                  Component.For<LeaveController>().LifestyleTransient(),
                   Component.For<HolidaysController>().LifestyleTransient(),
                    Component.For<EmployeeController>().LifestyleTransient()
                  );

            Container.Register(
                  Component.For<LeaveRequestQuery>().LifestyleTransient(),
                  Component.For<ListRolesQuery>().LifestyleTransient(),
                  Component.For<HolidaysQuery>().LifestyleTransient(),
                  Component.For<EmployeeQuery>().LifestyleTransient()
                  );

            //Register context classes for application EF and identity framework
            Container.Register(
                  Component.For<LAPPEntities>().LifestyleTransient(),
                  Component.For<ATTEREntities>().LifestyleTransient(),
                  Component.For<CustomIdentityDbContext>().LifestyleTransient()
                  );

            Container.Register(
                Component.For<PermissionService>().LifestyleTransient(),
                Component.For<EmployeeService>().LifestyleTransient(),
                Component.For<SendGridEmailNotificationService>().LifestyleTransient(),
                Component.For<EmailApplicationSettingsService>().LifestyleTransient(),
                 Component.For<LeaveService>().LifestyleTransient()
                );

            // register identity framework classes
            Container.Register(
                  Component.For<CustomIdentityUserManager>().LifestyleTransient(),
                  Component.For<CustomIdentitySignInManager>().LifestyleTransient(),
                  Component.For<CustomIdentityRoleManager>().LifestyleTransient()
                );

          

        }

        public Object Resolve(Type controllerType)
        {
            return Container.Resolve(controllerType);
        }

        public void Release(IController controller)
        {
            Container.Release(controller);
        }

    }
}