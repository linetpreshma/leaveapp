﻿using Infinite.LAPP.Log;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;


namespace Infinite.LAPP.WebFront.MVCInfrastructure.ExceptionFilters
{
    public class LoggingExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            System.Threading.Tasks.Task.Run(() => CustomLogger.Instance.LogErrorMessageAsync(
                ex: filterContext.Exception,
                userId: Convert.ToInt32(filterContext.HttpContext.User.Identity.GetUserId())
                ));
        }
    }
}