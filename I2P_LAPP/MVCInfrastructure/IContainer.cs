﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Infinite.LAPP.WebFront.MVCInfrastructure
{
    internal interface IContainer
    {
        void Register();

        Object Resolve(Type controllerType);

        void Release(IController controller);

    }
}