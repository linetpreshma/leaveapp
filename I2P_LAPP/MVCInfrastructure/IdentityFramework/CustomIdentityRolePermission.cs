﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Infinite.LAPP.WebFront.MVCInfrastructure.IdentityFramework
{
    [Table("RolePermissions", Schema = "dbo")]
    public class RolePermissionDefinition
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("CustomIdentityRole")]
        public int RoleId { get; set; }

        [ForeignKey("Module")]
        public int ModuleId { get; set; }

        public string AccessRights { get; set; }

        public virtual ModuleDefinition Module { get; set; }

        public virtual CustomIdentityRole CustomIdentityRole { get; set; }
    }
}