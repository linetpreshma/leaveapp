﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infinite.LAPP.WebFront.MVCInfrastructure.IdentityFramework
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class CustomIdentityRole : IdentityRole<int, CustomIdentityUserRole>
    {
        public string DisplayName { get; set; }

        public string Description { get; set; }

        //add property to handle list of permissions
        public virtual ICollection<RolePermissionDefinition> RolePermissions { get; set; }
    }
}