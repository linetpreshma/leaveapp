﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infinite.LAPP.WebFront.MVCInfrastructure.IdentityFramework
{
    public class CustomIdentityRoleManager : RoleManager<CustomIdentityRole, int>
    {
        public CustomIdentityRoleManager(IRoleStore<CustomIdentityRole, int> roleStore)
            : base(roleStore)
        {

        }
    }

    public class CustomIdentityRoleStore : RoleStore<CustomIdentityRole, int, CustomIdentityUserRole>
    {
        public CustomIdentityRoleStore(CustomIdentityDbContext context)
            : base(context)
        { }
    }
}