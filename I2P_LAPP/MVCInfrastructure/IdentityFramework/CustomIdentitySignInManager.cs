﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.DirectoryServices;
namespace Infinite.LAPP.WebFront.MVCInfrastructure.IdentityFramework
{
    public class CustomIdentitySignInManager : SignInManager<CustomIdentityUser, int>
    {
        //Enum to return User is InActive
        public enum CustomSignInStatus
        {
            IsNotActive = 4, // to handle logic for inactive users
        }
        public CustomIdentitySignInManager(CustomIdentityUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(CustomIdentityUser user)
        {
            return user.GenerateUserIdentityAsync((CustomIdentityUserManager)UserManager);
        }

        public override async Task<SignInStatus> PasswordSignInAsync(string userEmail, string password, bool isPersistent, bool shouldLockout)
        {
            //Check if user by userName.
            var user = await UserManager.FindByEmailAsync(userEmail);
            if (user != null && !user.IsActive)
            {
                return ((SignInStatus)CustomSignInStatus.IsNotActive);
            }

            //Check if credentials are correct.
            var result = await base.PasswordSignInAsync(userEmail, password, isPersistent, shouldLockout);

            //Reset access failed count to zero if login is successful.
            if (result == SignInStatus.Success)
            {
                await UserManager.ResetAccessFailedCountAsync(user.Id);
            }
            return (result);
        

            //// build UID string
            //String CN = "cn=" + userEmail + ",ou=Users,ou=Infinite-MDC,dc=ics,dc=global";
            //// assign password
          
            //// define LDAP connection
            //DirectoryEntry root = new DirectoryEntry(
            //    "LDAP://inmdcs0001.ics.global:389", CN, password,
            //    System.DirectoryServices.AuthenticationTypes.None);
            //var result = SignInStatus.Failure;
            //try
            //{

            //    // attempt to use LDAP connection
            //    object connected = root.NativeObject;

            //    // no exception, login successful
            //    result = SignInStatus.Success;
            //}
            //catch (Exception ex)
            //{
            //    // exception thrown, login failed
            //    result = SignInStatus.Failure;
            //}
            //return (result);
        }
    }
}