﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Infinite.LAPP.WebFront.MVCInfrastructure.IdentityFramework
{
    [Table("Modules", Schema = "dbo")]
    public class ModuleDefinition
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}