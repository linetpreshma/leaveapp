﻿using Infinite.LAPP.WebFront.MVCInfrastructure;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Infinite.LAPP.WebFront.MVCInfrastructure.IdentityFramework
{
    public class CustomIdentityUserManager : UserManager<CustomIdentityUser, int>
    {
       

        public CustomIdentityUserManager(IUserStore<CustomIdentityUser, int> store)
            : base(store)
        {


            // Configure validation logic for usernames
            this.UserValidator = new UserValidator<CustomIdentityUser, int>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            this.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 8,
                RequireDigit = true,
                RequireNonLetterOrDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            // Configure user lockout defaults
            this.UserLockoutEnabledByDefault = Convert.ToBoolean(ConfigurationManager.AppSettings["UserLockoutEnabledByDefault"].ToString()); ;
            this.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(Double.Parse(ConfigurationManager.AppSettings["DefaultAccountLockoutTimeSpan"].ToString()));
            this.MaxFailedAccessAttemptsBeforeLockout = Convert.ToInt32(ConfigurationManager.AppSettings["MaxFailedAccessAttemptsBeforeLockout"].ToString());

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            this.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<CustomIdentityUser, int>
            {
                MessageFormat = "Your security code is {0}"
            });
            this.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<CustomIdentityUser, int>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
            this.EmailService = new CustomIdentityEmailService();
            this.SmsService = new CustomIdentitySMSService();
            var dataProtectionProvider = Startup.DataProtectionProvider;

            if (dataProtectionProvider != null)
            {
                this.UserTokenProvider =
                    new DataProtectorTokenProvider<CustomIdentityUser, int>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
        }

       
    }

    public class CustomIdentityUserStore : UserStore<CustomIdentityUser, CustomIdentityRole, int, CustomIdentityUserLogin, CustomIdentityUserRole, CustomIdentityUserClaim>,
       IUserStore<CustomIdentityUser, int>, IDisposable
    {
        public CustomIdentityUserStore(CustomIdentityDbContext context)
            : base(context)
        { }

    }
}