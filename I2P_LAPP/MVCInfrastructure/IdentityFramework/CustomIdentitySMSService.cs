﻿using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace Infinite.LAPP.WebFront.MVCInfrastructure
{
    public class CustomIdentitySMSService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}