﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infinite.LAPP.WebFront.MVCInfrastructure.IdentityFramework
{
    public class CustomIdentityDbContext : IdentityDbContext<CustomIdentityUser, CustomIdentityRole, int, CustomIdentityUserLogin, CustomIdentityUserRole, CustomIdentityUserClaim>
    {
        public CustomIdentityDbContext()
            : base("DefaultConnection")
        {
        }
    }
}