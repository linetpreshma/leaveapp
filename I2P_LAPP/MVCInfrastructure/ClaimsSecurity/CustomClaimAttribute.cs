﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Services;
using System.Linq;
using System.Security;
using System.Security.Permissions;
using System.Web;


namespace Infinite.LAPP.WebFront.MVCInfrastructure.ClaimsSecurity
{
    /// <summary>
    /// Custom code access security class used to set the rescource value and associated claim for an action.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property, AllowMultiple = true)]
    public sealed class CustomClaimAttribute : CodeAccessSecurityAttribute
    {
        public string Value { get; set; }
        public string Resource { get; set; }

        public CustomClaimAttribute(SecurityAction action)
            : base(action)
        {
        }

        /// <summary>
        /// This method is used to define claim for a particular resource.
        /// </summary>
        /// <returns></returns>
        public override IPermission CreatePermission()
        {
            return new ClaimsPrincipalPermission(Resource, Value);
        }
    }
}