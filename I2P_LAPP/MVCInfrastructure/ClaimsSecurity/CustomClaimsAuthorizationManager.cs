﻿
using System.Linq;
using System.Security.Claims;

namespace Infinite.LAPP.WebFront.MVCInfrastructure.ClaimsSecurity
{

    /// <summary>
    /// This class is used to define the implementation for a claims authorization manager.
    /// </summary>
    public class CustomClaimsAuthorizationManager : ClaimsAuthorizationManager
    {
        /// <summary>
        /// This method is used to check access on a particular resource for the user existing in the current context (Principal)
        /// </summary>
        /// <param name="context">authorization context which contains the identity and claims of the logged in user</param>
        /// <returns>true or false based on user claims</returns>
        public override bool CheckAccess(AuthorizationContext context)
        {
            var action = context.Action.First().Value;
            var resource = context.Resource.First().Value;
            var retVal = context.Principal.HasClaim(resource, action);

            return retVal;
        }
    }
}