﻿using Infinite.LAPP.WebFront.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Infinite.LAPP.WebFront.HtmlHelpers
{
    public static class CustomHtmlExtensions
    {
        public static MvcHtmlString HeaderSectionHtml(this HtmlHelper htmlHelper, string headerText)
        {
            //build outer div tag 
            var outerDivTagBuilder = new TagBuilder("div");
            outerDivTagBuilder.MergeAttribute("class", "lapp-content-header");
            //build H2 tag
            var hTagBuilder = new TagBuilder("h2");
            hTagBuilder.MergeAttribute("class", "admin-head");

            //merging content div inside h 
            hTagBuilder.InnerHtml += headerText;
                
            outerDivTagBuilder.InnerHtml += hTagBuilder.ToString();


            return new MvcHtmlString(outerDivTagBuilder.ToString());

        }

        public static MvcHtmlString TaskLinkHtml(this HtmlHelper htmlHelper, string taskName, string taskUrl, string iconClass, bool isRenderResultOnPopUp = false, string callBackUrl = null,
                                                 string popupDisplayMessage = null, string popupTitle = null, string id = "actionButton")
        {
            ////build div tag 
            //var divTagBuilder = new TagBuilder("div");
            //divTagBuilder.MergeAttribute("class", "btn btn-primary");

            //build A tag
            var aTagBuilder = new TagBuilder("div");

            aTagBuilder.MergeAttribute("data-url", taskUrl);
            aTagBuilder.MergeAttribute("class", "btn btn-primary mar-left-md ");
            aTagBuilder.MergeAttribute("id", id);

            if (popupDisplayMessage != null)
            {
                aTagBuilder.MergeAttribute("data-popupDisplayMessage", popupDisplayMessage);
            }

            if (popupTitle != null)
            {
                aTagBuilder.MergeAttribute("data-popupTitle", popupTitle);
            }

            if (isRenderResultOnPopUp)
            {
                aTagBuilder.MergeAttribute("data-callbackurl", callBackUrl);
                aTagBuilder.MergeAttribute("data-renderpopup", Convert.ToString(isRenderResultOnPopUp));
            }

            if (iconClass != null)
            {
                //build I tag
                var iTagBuilder = new TagBuilder("i");
                iTagBuilder.MergeAttribute("class", iconClass);

                //merging I tag and task name to A tag
                aTagBuilder.InnerHtml += iTagBuilder.ToString();
                aTagBuilder.InnerHtml += "&nbsp";
            }

            aTagBuilder.InnerHtml += taskName;

            ////merging A tag to div tag
            //divTagBuilder.InnerHtml += aTagBuilder;

            return new MvcHtmlString(aTagBuilder.ToString());

        }

        //public static MvcHtmlString PopupHtml(this HtmlHelper htmlHelper, string taskName, string taskUrl, string callBackUrl = null, string popupDisplayMessage = null, string popupTitle = null, string id = "actionButton")
        //{
        //    //build div tag 
        //    var divTagBuilder = new TagBuilder("div");
        //    divTagBuilder.MergeAttribute("class", "item");

        //    //build A tag
        //    var aTagBuilder = new TagBuilder("a");
        //    aTagBuilder.MergeAttribute("class", "finger-pointer-for-non-link");
        //    aTagBuilder.MergeAttribute("data-url", taskUrl);
        //    aTagBuilder.MergeAttribute("id", id);

        //    if (popupDisplayMessage != null)
        //    {
        //        aTagBuilder.MergeAttribute("data-popupDisplayMessage", popupDisplayMessage);
        //    }

        //    if (popupTitle != null)
        //    {
        //        aTagBuilder.MergeAttribute("data-popupTitle", popupTitle);
        //    }


        //    aTagBuilder.MergeAttribute("data-callbackurl", callBackUrl);
        //    aTagBuilder.MergeAttribute("data-renderpopup", Convert.ToString(true));

        //    aTagBuilder.InnerHtml += taskName;

        //    //merging A tag to div tag
        //    divTagBuilder.InnerHtml += aTagBuilder;

        //    return new MvcHtmlString(divTagBuilder.ToString());

        //}

        public static MvcHtmlString CancelButtonHtml(this HtmlHelper htmlHelper, string id = "cancelButton", bool isPopUpCancelButton = false)
        {
            //build div tag 
            var cancelDivTagBuilder = new TagBuilder("div");
            cancelDivTagBuilder.MergeAttribute("class", "btn btn-primary");
            cancelDivTagBuilder.MergeAttribute("tabindex", "0");
            cancelDivTagBuilder.MergeAttribute("id", id);

            if (isPopUpCancelButton)
            {
                cancelDivTagBuilder.MergeAttribute("data-popupcancelbutton", Convert.ToString(isPopUpCancelButton));
            }
            //build visible div inside cancel div
            var cancelVisibleDivTagBuilder = new TagBuilder("div");
            cancelVisibleDivTagBuilder.MergeAttribute("class", "view-content");
            cancelVisibleDivTagBuilder.InnerHtml = "Cancel";

            //build hidden content div for cancel
            var cancelHiddenDivTagBuilder = new TagBuilder("div");
            cancelHiddenDivTagBuilder.MergeAttribute("class", "hide-content");

            //build remove icon for cancel button
            var iTagBuilder = new TagBuilder("i");
            iTagBuilder.MergeAttribute("class", "fa fa-times");

            cancelHiddenDivTagBuilder.InnerHtml += iTagBuilder.ToString();


            cancelDivTagBuilder.InnerHtml += cancelVisibleDivTagBuilder.ToString();
            cancelDivTagBuilder.InnerHtml += cancelHiddenDivTagBuilder.ToString();

            return new MvcHtmlString(cancelDivTagBuilder.ToString());
        }

        public static MvcHtmlString SaveButtonHtml(this HtmlHelper htmlHelper, string saveButtonText, string mode, string entityName, string actionButtonList = null,
                                                   string popupDisplayMessage = null, string popupTitle = null, bool isPopUpSaveButton = false, string redirectUrl = null, string btnIcon = "fa fa-check ")
        {

            //build div tag 
            var saveDivTagBuilder = new TagBuilder("div");
            saveDivTagBuilder.MergeAttribute("class", "btn btn-success");
            saveDivTagBuilder.MergeAttribute("tabindex", "0");
            saveDivTagBuilder.MergeAttribute("id", "saveButton");
            if (actionButtonList != null)
            {
                saveDivTagBuilder.MergeAttribute("data-mode", mode);
                saveDivTagBuilder.MergeAttribute("data-entity", entityName);
                saveDivTagBuilder.MergeAttribute("data-buttons", actionButtonList);
            }

            if (redirectUrl != null)
            {
                saveDivTagBuilder.MergeAttribute("data-redirecturl", redirectUrl);
            }

            if (popupDisplayMessage != null)
            {
                saveDivTagBuilder.MergeAttribute("data-popupDisplayMessage", popupDisplayMessage);
            }

            if (popupTitle != null)
            {
                saveDivTagBuilder.MergeAttribute("data-popupTitle", popupTitle);
            }

            if (isPopUpSaveButton)
            {
                saveDivTagBuilder.MergeAttribute("data-popupsavebutton", Convert.ToString(isPopUpSaveButton));
            }

            //build visible div inside save div
            var saveVisibleDivTagBuilder = new TagBuilder("div");
            saveVisibleDivTagBuilder.MergeAttribute("class", "view-content");
            saveVisibleDivTagBuilder.InnerHtml = saveButtonText;

            //build hidden content div for save
            var saveHiddenDivTagBuilder = new TagBuilder("div");
            saveHiddenDivTagBuilder.MergeAttribute("width", "inherit");
            saveHiddenDivTagBuilder.MergeAttribute("class", "hide-content");

            //build remove icon for save button
            var iTagBuilder = new TagBuilder("i");
            iTagBuilder.MergeAttribute("class", btnIcon);

            saveHiddenDivTagBuilder.InnerHtml += iTagBuilder.ToString();


            saveDivTagBuilder.InnerHtml += saveVisibleDivTagBuilder.ToString();
            saveDivTagBuilder.InnerHtml += saveHiddenDivTagBuilder.ToString();

            return new MvcHtmlString(saveDivTagBuilder.ToString());
        }

        //public static MvcHtmlString DeleteLinkHtml(this HtmlHelper htmlHelper, string iconClass, string linkText = null, string actionButtonList = null,
        //                                           string popupDisplayMessage = null, string popupTitle = null)
        //{
        //    //build div tag 
        //    var divTagBuilder = new TagBuilder("div");
        //    divTagBuilder.MergeAttribute("class", "btn-warning");
        //    divTagBuilder.MergeAttribute("id", "deleteButton");
        //    divTagBuilder.MergeAttribute("data-content", "Delete");
        //    if (actionButtonList != null)
        //    {
        //        divTagBuilder.MergeAttribute("data-buttons", actionButtonList);
        //    }
        //    if (popupDisplayMessage != null)
        //    {
        //        divTagBuilder.MergeAttribute("data-popupDisplayMessage", popupDisplayMessage);
        //    }

        //    if (popupTitle != null)
        //    {
        //        divTagBuilder.MergeAttribute("data-popupTitle", popupTitle);
        //    }

        //    if (iconClass != null)
        //    {
        //        //build I tag
        //        var iTagBuilder = new TagBuilder("i");
        //        iTagBuilder.MergeAttribute("class", iconClass);

        //        if (linkText != null)
        //        {
        //            iTagBuilder.InnerHtml = linkText;
        //        }

        //        //merging I tag and task name to A tag
        //        divTagBuilder.InnerHtml += iTagBuilder.ToString();
        //    }

        //    return new MvcHtmlString(divTagBuilder.ToString());
        //}

        //public static MvcHtmlString EditLinkHtml(this HtmlHelper htmlHelper, string taskUrl, string iconClass, string linkText = null, bool isRenderResultOnPopUp = false, string callBackUrl = null,
        //                                         string popupDisplayMessage = null, string popupTitle = null, string id = "actionButton")
        //{
        //    //build div tag 
        //    var divTagBuilder = new TagBuilder("div");
        //    divTagBuilder.MergeAttribute("class", "ui olive button");
        //    divTagBuilder.MergeAttribute("id", id);

        //    divTagBuilder.MergeAttribute("data-url", taskUrl);
        //    divTagBuilder.MergeAttribute("data-content", "Edit");

        //    if (popupDisplayMessage != null)
        //    {
        //        divTagBuilder.MergeAttribute("data-popupDisplayMessage", popupDisplayMessage);
        //    }

        //    if (popupTitle != null)
        //    {
        //        divTagBuilder.MergeAttribute("data-popupTitle", popupTitle);
        //    }

        //    if (isRenderResultOnPopUp)
        //    {
        //        divTagBuilder.MergeAttribute("data-callbackurl", callBackUrl);
        //        divTagBuilder.MergeAttribute("data-renderpopup", Convert.ToString(isRenderResultOnPopUp));
        //    }


        //    if (iconClass != null)
        //    {
        //        //build I tag
        //        var iTagBuilder = new TagBuilder("i");
        //        iTagBuilder.MergeAttribute("class", iconClass);

        //        if (linkText != null)
        //        {
        //            iTagBuilder.InnerHtml = linkText;
        //        }

        //        //merging I tag and task name to A tag
        //        divTagBuilder.InnerHtml += iTagBuilder.ToString();
        //    }

        //    return new MvcHtmlString(divTagBuilder.ToString());
        //}

        //public static MvcHtmlString ToggleButton(this HtmlHelper htmlHelper, string btnId, string btnClass, bool value, string value1Text, string value1HoverText, string value2Text, string value2HoverText, string targertElementId = null, bool showDiv = true)
        //{
        //    // html for button
        //    var buttonTagBuilder = new TagBuilder("div");
        //    buttonTagBuilder.MergeAttribute("class", btnClass);
        //    buttonTagBuilder.MergeAttribute("id", btnId);
        //    if (showDiv)
        //    {
        //        buttonTagBuilder.MergeAttribute("style", "display:block");
        //    }
        //    else
        //    {
        //        buttonTagBuilder.MergeAttribute("style", "display:none");
        //    }
        //    if (string.IsNullOrEmpty(targertElementId))
        //    {
        //        buttonTagBuilder.MergeAttribute("onclick", "onToggleButtonClick('" + btnId + "', " + Convert.ToString(value).ToLower() + ")");
        //    }
        //    else
        //    {
        //        buttonTagBuilder.MergeAttribute("onclick", "onToggleButtonClick('" + btnId + "', " + Convert.ToString(value).ToLower() + ",'" + targertElementId + "')");
        //    }
        //    // html for visible content div
        //    var visibleContentTagBuilder = new TagBuilder("div");
        //    visibleContentTagBuilder.MergeAttribute("class", "visible content");
        //    if (value)
        //        visibleContentTagBuilder.InnerHtml = value1Text;
        //    else
        //        visibleContentTagBuilder.InnerHtml = value2Text;

        //    // html for hidden content div
        //    var hiddenContentTagBuilder = new TagBuilder("div");
        //    hiddenContentTagBuilder.MergeAttribute("class", "hidden grey content");
        //    if (value)
        //        hiddenContentTagBuilder.InnerHtml = value1HoverText;
        //    else
        //        hiddenContentTagBuilder.InnerHtml = value2HoverText;

        //    buttonTagBuilder.InnerHtml += visibleContentTagBuilder;

        //    buttonTagBuilder.InnerHtml += hiddenContentTagBuilder;

        //    return new MvcHtmlString(buttonTagBuilder.ToString());
        //}

        public static MvcHtmlString CurrencyValue(this HtmlHelper htmlHelper, decimal value)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture(DisplayFormatConstants.CurrentCulture);
            culture.NumberFormat.CurrencyNegativePattern = 1;
            String str = String.Format(culture, DisplayFormatConstants.CurrencyFormatString, value);

            return new MvcHtmlString(str.ToString());
        }
    }
}