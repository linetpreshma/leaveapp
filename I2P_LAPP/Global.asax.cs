﻿using Infinite.LAPP.Repository.Dependencies;
using Infinite.LAPP.WebFront.MVCInfrastructure;
using Infinite.LAPP.WebFront.MVCInfrastructure.DIContainer;
using Infinite.LAPP.WebFront.MVCInfrastructure.IdentityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Infinite.LAPP.WebFront
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var autoMapper = new AutoMapperRegistry(new CustomUserSession());
            autoMapper.Register();

            var container = new CastleWindsorContainer();
            container.Register();

            var controllerFactory = new ControllerFactory(container);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            //JobScheduler.Start();
        }

        protected void Application_BeginRequest()
        {
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
                Response.Cache.SetNoStore();
            
        }
    }
}
