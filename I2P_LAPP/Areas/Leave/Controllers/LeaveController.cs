﻿using AutoMapper;
using Infinite.LAPP.Domain;
using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.Domain.IRepository;
using Infinite.LAPP.Domain.Service;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.Repository.QueryObjects;
using Infinite.LAPP.WebFront.Areas.Leave.Models;
using Infinite.LAPP.WebFront.Areas.Settings.Models;
using Infinite.LAPP.WebFront.Controllers;
using Infinite.LAPP.WebFront.MVCInfratructure.MappingLayer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Infinite.LAPP.WebFront.Areas.Leave.Controllers
{
    public class LeaveController : BaseController
    {
        private LeaveRequestQuery _query;
        private HolidaysQuery hldQuery;
        private readonly LeaveService _lserve;
        private LeaveRequestQuery _lQuery;
        private readonly ILeaveRepository _lRep;
        public readonly EmployeeQuery _empQuery;

        public LeaveController(HolidaysQuery _hldQuery, LeaveRequestQuery query, ILeaveRepository lRep, EmployeeQuery empQuery, LeaveRequestQuery lQuery, LeaveService lserve)
        {
            if (_hldQuery == null)
                throw new ArgumentNullException("_hldQuery is null");
            if (lRep == null)
                throw new ArgumentNullException("lRep is null");
            if (empQuery == null)
                throw new ArgumentNullException("empQuery is null");
            if (lQuery == null)
                throw new ArgumentNullException("lQuery is null");
            if (lserve == null)
                throw new ArgumentNullException("lserve is null");
            if (query == null)
                throw new ArgumentNullException("query is null");
            hldQuery = _hldQuery;
            _empQuery = empQuery;
            _lRep = lRep;
            _lQuery = lQuery;
            _lserve = lserve;
            _query= query;



        }

        // GET: Leave/Leave
        public ActionResult ApplyLeave()
        {
            var employee = _empQuery.GetEmployeeDetailByUserId(base.LoggedInUserId);
            var leave = new CreateLeaveRequestViewModel()
                {
                    LeaveType = (LeaveType)1,
                    EmployeeCode = employee.EmployeeCode,
                    EmployeeId = employee.Id,
                    EmployeeName = employee.FirstName + " " + employee.LastName,
                    Category = (Infinite.LAPP.Domain.Category)1
                };
            return View(leave);
        }



        [HttpPost]
        public ActionResult Applyleave(CreateLeaveRequestViewModel model)
        {
            var leaveRequest = Mapper.Map<CreateLeaveRequestViewModel, LeaveRequestDomainModel>(model);
            _lserve.InsertLeave(leaveRequest);
            return RedirectToAction("LeaveList");
        }

        public ActionResult EditLeaveGetDates(string From, string To, int LeaveId)
        { 
            //var leaveRequestDetails = _query.GetLeaveRequestById(LeaveId);

            var employee = _empQuery.GetEmployeeDetailByUserId(base.LoggedInUserId);
            var holidays = hldQuery.getAllHolidays();

            ArrayList HolidayList = new ArrayList();
            foreach (var item in holidays)
            {
                if (item.Location == (int)employee.Location)
                    HolidayList.Add(item.Date);
            }

            DateTime firstDay = Convert.ToDateTime(From);//, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            DateTime lastDay = Convert.ToDateTime(To); //DateTime.ParseExact(To, "dd-MM-yyyy", CultureInfo.InvariantCulture);

            var dayList = new List<Dates>();
            if (employee.Trainee == true)
            {
                for (firstDay = firstDay; firstDay <= lastDay; firstDay = firstDay.AddDays(1))
                {
                    bool compare = HolidayList.Contains(firstDay);
                    if (firstDay.DayOfWeek != DayOfWeek.Sunday && compare == false)
                    {
                        dayList.Add(new Dates() { Date = firstDay, Duration = (Duration)2 });
                    }
                }
            }
            else
            {
                for (firstDay = firstDay; firstDay <= lastDay; firstDay = firstDay.AddDays(1))
                {
                    bool compare = HolidayList.Contains(firstDay);
                    if (firstDay.DayOfWeek != DayOfWeek.Saturday && firstDay.DayOfWeek != DayOfWeek.Sunday && compare == false)
                    {
                        dayList.Add(new Dates() { Date = firstDay, Duration = (Duration)2 });
                    }
                }
            }


            var mainmodel = new EditLeaveViewModel() { DayList = dayList };

            return PartialView("_EditLeaveDetails", mainmodel);
        }


        public ActionResult GetDates(string From, string To)
        {
            var employee = _empQuery.GetEmployeeDetailByUserId(base.LoggedInUserId);
            var holidays = hldQuery.getAllHolidays();

            ArrayList HolidayList = new ArrayList();
            foreach (var item in holidays)
            {
                if(item.Location == (int)employee.Location)
                HolidayList.Add(item.Date);
            }

            DateTime firstDay = Convert.ToDateTime(From);//, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            DateTime lastDay = Convert.ToDateTime(To); //DateTime.ParseExact(To, "dd-MM-yyyy", CultureInfo.InvariantCulture);

            var dayList = new List<Dates>();
            if (employee.Trainee == true)
            {
                for (firstDay = firstDay; firstDay <= lastDay; firstDay = firstDay.AddDays(1))
                {
                    bool compare = HolidayList.Contains(firstDay);
                    if ( firstDay.DayOfWeek != DayOfWeek.Sunday && compare == false)
                    {
                        dayList.Add(new Dates() { Date = firstDay, Duration = (Duration)2 });
                    }
                }
            }
            else
            {
                for (firstDay = firstDay; firstDay <= lastDay; firstDay = firstDay.AddDays(1))
                {
                    bool compare = HolidayList.Contains(firstDay);
                    if (firstDay.DayOfWeek != DayOfWeek.Saturday && firstDay.DayOfWeek != DayOfWeek.Sunday && compare == false)
                    {
                        dayList.Add(new Dates() { Date = firstDay, Duration = (Duration)2 });
                    }
                }
            }


            var mainmodel = new CreateLeaveRequestViewModel() { DayList = dayList };

            return PartialView("Getdates", mainmodel);
        }


        public bool checkdates(DateTime firstdate, ArrayList HolidayList)
        {
            foreach (var item in HolidayList)
            {
                if (item.Equals(firstdate))
                    return false;
            }
            return true;
        }

        [MappingFilter(typeof(List<vw_GetAllLeavesList>), typeof(List<DisplayLeaveRequestDetailsModel>))]
        public ActionResult LeaveList()
        {
            var employee = _empQuery.GetEmployeeDetailByUserId(base.LoggedInUserId);
            var model = _lQuery.GetLeaveRequestByEmployeeId(employee.Id).OrderByDescending(x => x.CreatedDate).ToList();
            
            return View("LeaveList",model);
            
        }

        [MappingFilter(typeof(List<vw_GetAllLeavesList>), typeof(List<DisplayLeaveRequestDetailsModel>))]
        public ActionResult AllLeaveList()
        {
            var employee = _empQuery.GetEmployeeDetailByUserId(base.LoggedInUserId);
            var model = _lQuery.GetAllLeaveRequest().Where(y => y.Id != employee.Id).OrderByDescending(x => x.CreatedDate).ToList();
            
            return View("AllLeaveList", model);
           
        }

        public ActionResult GetLeaveCategories(int leaveTypeId)
        {
            var categories = _lQuery.GetAllCategoriesByLeaveTypeId(leaveTypeId);
            return Json(categories, JsonRequestBehavior.AllowGet);
        }

        [MappingFilter(typeof(List<vw_GetAllLeavesListwithDetails>), typeof(List<LeaveViewModel>))]
        public ActionResult GetLeaveListThisMonth()
        {
            var date = DateTime.Now;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            var leaves = _lQuery.GetLeaveListThisMonth(base.LoggedInUserId, firstDayOfMonth, lastDayOfMonth);
            return PartialView("_LeaveListThisMonthPartialView", leaves);

        }

         [MappingFilter(typeof(LeaveRequest), typeof(EditLeaveViewModel))]
        public ActionResult ViewLeaveDetails(int LeaveRequestId)
        {
            var leaveRequestDetails = _query.GetLeaveRequestById(LeaveRequestId);
            return View(leaveRequestDetails);

        }

         [MappingFilter(typeof(LeaveRequest), typeof(EditLeaveViewModel))]
         public ActionResult EmployeeLeaveDetails(int LeaveRequestId)
         {
             var leaveRequestDetails = _query.GetLeaveRequestById(LeaveRequestId);
             return View("EmployeeLeaveDetails",leaveRequestDetails);

         }


         [MappingFilter(typeof(LeaveRequest), typeof(EditLeaveViewModel))]
        public ActionResult EditLeave(int Id)
        {
            var leaveRequestDetails = _query.GetLeaveRequestById(Id);
            return View(leaveRequestDetails);
        }



        [HttpPost]
         public ActionResult EditLeave(EditLeaveViewModel model)
         {

             _lRep.EditLeave(Mapper.Map<EditLeaveViewModel, EditLeaveDomainModel>(model));
             return RedirectToAction("LeaveList");
         }

        //public ActionResult CancelLeave(int Id)
        //{
        //    var leaveRequestDetails = _query.GetLeaveRequestById(Id);
        //    return View(leaveRequestDetails);
        //}



        [HttpPost]
        public ActionResult CancelLeave(int LeaveRequestId)
        {
            _lserve.CancelLeave(LeaveRequestId);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

    }
}
