﻿using Infinite.LAPP.WebFront.Areas.Leave.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Infinite.LAPP.Repository.QueryObjects;
using Infinite.LAPP.WebFront.Controllers;
using AutoMapper;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.WebFront.MVCInfratructure.MappingLayer;
using Infinite.LAPP.Domain.IRepository;
using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.Domain.Service;
using Infinite.LAPP.WebFront.MVCInfrastructure.ClaimsSecurity;
using System.Security.Permissions;

namespace Infinite.LAPP.WebFront.Areas.Leave.Controllers
{
    public class ManageLeavesController : BaseController
    {
        private LeaveRequestQuery _query = null;
        private LeaveService _leaveRep = null; 

        public ManageLeavesController(LeaveRequestQuery query, LeaveService leaveRep)
        {
            if (leaveRep == null)
                throw new ArgumentNullException("leaveRep is null");
            if (query == null)
                throw new ArgumentNullException("query is null");
            _leaveRep = leaveRep;
            _query = query;

        }

          // GET: Leave/ManageLeaves
        [MappingFilter(typeof(List<vw_GetAllLeavesListForApproval>), typeof(List<ApproveLeaveRequestDetailsModel>))]
        [CustomClaimAttribute(SecurityAction.Demand, Resource = "Leave Management", Value = "canApproveLeave")]
        public ActionResult Index()
        {
            var model = _query.GetAllLeavesByUserId(base.LoggedInUserId);
            return View(model);
        }



        // GET: Leave/ManageLeaves/Details/5
        [MappingFilter(typeof(LeaveRequest), typeof(ManageLeavesViewModel))]
        [CustomClaimAttribute(SecurityAction.Demand, Resource = "Leave Management", Value = "canApproveLeave")]
        public ActionResult LeaveDetails(int leaveRequestId)
        {
            var leaveRequestDetails = _query.GetLeaveRequestById(leaveRequestId);
            return View(leaveRequestDetails);
        }

        public ActionResult ApproveLeave(ManageLeavesViewModel model)
        {
           
            var domainModel = new ApproveLeaveDomainModel()
            {
                LeaveRequestId = model.Id,
                 EmployeeId= model.EmployeeId,
                 DayList= Mapper.Map<List<Dates>,List<DatesDomainModel>>(model.DayList),
                 ApproverComment = model.ApproverComment,
                 Status = 1
            };
            _leaveRep.ApproveLeaveRequest(domainModel);
            return RedirectToAction("Index");

        }

        public ActionResult RejectLeave(int Id, string ApproverComment)
        {
            _leaveRep.RejectLeave(Id, ApproverComment);
            return RedirectToAction("Index");
        }
    }
}
