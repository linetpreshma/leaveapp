﻿using Infinite.LAPP.Domain;
using Infinite.LAPP.WebFront.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Infinite.LAPP.WebFront.Areas.Leave.Models
{
    public class ManageLeavesViewModel
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public List<Dates> DayList { get; set; }
        public Category Category { get; set; }
        public int EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [MaxLength(40, ErrorMessage = "Comment cannot be longer than 40 characters.")]
        public string ApproverComment { get; set; }
        public string ApplierComment { get; set; }
        public string ApproverName { get; set; }
        public LeaveType Type { get; set; }
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString)]
        public System.DateTime FromDate { get; set; }
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString)]
        public System.DateTime ToDate { get; set; }
        public int Status { get; set; }
        public int ReportingManagerId { get; set; }
    }

    public class DisplayLeaveRequestDetailsModel 
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString)]
        public Nullable<System.DateTime> FromDate { get; set; }
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString)]
        public Nullable<System.DateTime> ToDate { get; set; }
        public LeaveType LeaveType { get; set; }
        public Category Category { get; set; }
        public string CategoryName { get; set; }
        public string Comments { get; set; }
        public int Status { get; set; }
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString)]
        public DateTime CreatedDate { get; set; }
        public string ReportingManagerName { get; set; }
        public bool IsDeleted { get; set; }

    }

    public class ApproveLeaveRequestDetailsModel
    {


        public string EmployeeName { get; set; }
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString)]
        public Nullable<System.DateTime> FromDate { get; set; }
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString)]
        public Nullable<System.DateTime> ToDate { get; set; }
        public LeaveType LeaveType { get; set; }
        public Category Category { get; set; }
        public string CategoryName { get; set; }
        public int EmployeeCode { get; set; }
        public string Comments { get; set; }
        public int Status { get; set; }
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString)]
        public DateTime CreatedDate { get; set; }
    }

}