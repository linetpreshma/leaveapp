﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infinite.LAPP.WebFront.Areas.Leave.Models
{
    public class LeaveDetailsViewModel
    {
        public int Id { get; set; }
        public string ApproverComment { get; set; }
        public int Category { get; set; }
        public int EmployeeId { get; set; }
        public string Comments { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsDeleted { get; set; }
        public int ReportingMangerId { get; set; }
        public string ReportingManagerName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public Nullable<int> LeaveRequestStatus { get; set; }
        public int Type { get; set; }
        public string LeaveType { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> Duration { get; set; }
        public Nullable<int> LeaveRequestDetailsId { get; set; }
        public Nullable<int> DetailsStatus { get; set; }
        public string CategoryName { get; set; }
        public Nullable<int> UserId { get; set; }
    }
}