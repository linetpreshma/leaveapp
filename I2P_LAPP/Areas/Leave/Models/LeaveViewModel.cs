﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Infinite.LAPP.Domain;
using System.ComponentModel;
using Infinite.LAPP.Domain.DomainModel;
using System.ComponentModel.DataAnnotations;
using Infinite.LAPP.WebFront.Common;

namespace Infinite.LAPP.WebFront.Areas.Leave.Models
{
    public class LeaveViewModel
    {
        public int Id { get; set; }
        public int Category { get; set; }
        public int EmployeeId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ReportingMangerId { get; set; }
        public string ReportingManagerName { get; set; }
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString)]
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> Duration { get; set; }
        public string CategoryName { get; set; }
        public Nullable<int> UserId { get; set; }

    }

    public class EditLeaveViewModel
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public List<Dates> DayList { get; set; }
        public Category Category { get; set; }
        public int EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string ApproverComment { get; set; }
        [DisplayName("Comments")]
        [MaxLength(40, ErrorMessage = "Comment cannot be longer than 40 characters.")]
        public string ApplierComment { get; set; }
        public string ApproverName { get; set; }
        [DisplayName("Leave Type")]
        public LeaveType Type { get; set; }
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString,ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [DisplayName("From Date")]
        public System.DateTime FromDate { get; set; }
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString,ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [DisplayName("To Date")]
        public System.DateTime ToDate { get; set; }
        public int Status { get; set; }
        public int ReportingManagerId { get; set; }
    }

    public class CreateLeaveRequestViewModel : EmployeeDetailModel
    {
        public int Id { get; set; }

        [DisplayName("From Date")]
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString)]
        [Required(ErrorMessage= ValidationMessages.RequiredValidationMessage)]
        public Nullable<System.DateTime> FromDate { get; set; }
        [DisplayName("To Date")]
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString)]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        public Nullable<System.DateTime> ToDate { get; set; }
        [DisplayName("Category")]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        public Category Category { get; set; }
        [DisplayName("Leave Type")]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        public LeaveType LeaveType { get; set; }
        public List<Dates> DayList { get; set; }
        [DisplayName("Comments")]
        [MaxLength(40, ErrorMessage = "Comment cannot be longer than 40 characters.")]
        public string ApplierComment { get; set; }
        public int Status { get; set; }

    }
    public class EmployeeDetailModel
    {
        public int EmployeeId { get; set; }
        [DisplayName("Employee Name")]
        public string EmployeeName { get; set; }
        [DisplayName("Employee Code")]
        public int EmployeeCode { get; set; }
    }

    public class Dates 
    {
        public int Id { get; set; }
        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString)]
        public DateTime Date { get; set; }
        [DisplayName("Leave Duration")]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        public Duration Duration { get; set; }
        public int Status { get; set; }
        public Nullable<bool> IsDeleted { get; set; }

    }

}