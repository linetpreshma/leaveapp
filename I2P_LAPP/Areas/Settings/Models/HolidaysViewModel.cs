﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Infinite.LAPP.Domain;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Infinite.LAPP.WebFront.Common;
using Infinite.LAPP.WebFront.Models;

namespace Infinite.LAPP.WebFront.Areas.Settings.Models
{
    public class HolidaysViewModel 
    {
        public int ID { get; set; }
         [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        public string Title { get; set; }
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString, ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        public DateTime Date { get; set; }
        [DisplayName("Description")]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [MaxLength(40, ErrorMessage = "Desciption cannot be longer than 40 characters.")]
        public string Desciption { get; set; }
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [Range(1, int.MaxValue, ErrorMessage = "Please select a Location")]
        public Location Location { get; set; }

        public ClaimsModel Claim { get; set; }

    }
}