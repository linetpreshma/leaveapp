﻿using AutoMapper;
using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.Domain.IRepository;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.Repository.QueryObjects;
using Infinite.LAPP.WebFront.Areas.Settings.Models;
using Infinite.LAPP.WebFront.Controllers;
using Infinite.LAPP.WebFront.Models;
using Infinite.LAPP.WebFront.MVCInfrastructure.ClaimsSecurity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web;
using System.Web.Mvc;

namespace Infinite.LAPP.WebFront.Areas.Settings.Controllers
{
    public class HolidaysController : BaseController
    {
        private IHolidaysRepository _holRep;
        private HolidaysQuery _hQuery;

        public HolidaysController(IHolidaysRepository holRep, HolidaysQuery hQuery)
        {
            if (holRep == null)
                throw new ArgumentNullException("holRep is null");
            if (hQuery == null)
                throw new ArgumentNullException("hQuery is null");
            _holRep = holRep;
            _hQuery = hQuery;
        }

        // GET: Settings/Holidays
        [CustomClaimAttribute(SecurityAction.Demand, Resource = "Holiday Management", Value = "canAddHoliday")]
        public ActionResult AddHolidays()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddHolidays(HolidaysViewModel Holiday)
        {
          var Holidays = Mapper.Map<HolidaysViewModel , HolidaysDomainModel>(Holiday);
          int id =  _holRep.Insert(Holidays);

          return RedirectToAction("AddHolidays");
        }

        public ActionResult HolidayList()
        {
            var holiday = new HolidaysViewModel() { Claim = new ClaimsModel() };
            if(holiday.Claim.CanAddHoliday==true)
            {
                return View("HolidayList", holiday);
            }
            else
            {
                return View("EmployeeHolidayList", holiday);
            }
        }

        public JsonResult SearchHolidayList(int locationId, int year)
        {
            DateTime dt = new DateTime(year, 1, 1);
            var Holidays = Mapper.Map<List<Holiday>, List<HolidaysViewModel>>(_hQuery.GetHolidaysByLocation(locationId,dt)).ToList();
            return Json(Holidays, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteHoliday(int Id)
        {
            _holRep.DeleteHoliday(Id);
            return RedirectToAction("HolidayList");
        }

        public ActionResult EditHoliday(int Id)
        {
            var Holiday = Mapper.Map<Holiday, HolidaysViewModel>(_hQuery.GetHolidaysById(Id));
            return View(Holiday);
        }


        public ActionResult UpdateHoliday(HolidaysViewModel Holiday)
        {
            var Holidays = Mapper.Map<HolidaysViewModel, HolidaysDomainModel>(Holiday);
             _holRep.UpdateHoliday(Holidays);
            return RedirectToAction("HolidayList");
        }
      
    }
}
