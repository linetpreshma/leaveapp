﻿using Infinite.LAPP.Domain;
using Infinite.LAPP.WebFront.Common;
using System;
using Infinite.LAPP.WebFront.Areas.Users.Models;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Infinite.LAPP.WebFront.Areas.Employee.Models
{
    public class EmployeeViewModel
    {
        public int Id { get; set; }
        [DisplayName("First Name")]
        [RegularExpression(ValidationRegex.NameFormat, ErrorMessage = ValidationMessages.NameErrorMessage)]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [StringLength(25, ErrorMessage = ValidationMessages.LengthLimit)]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [RegularExpression(ValidationRegex.NameFormat, ErrorMessage = ValidationMessages.NameErrorMessage)]
        [StringLength(25, ErrorMessage = ValidationMessages.LengthLimit)]
        public string LastName { get; set; }

        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [Range(1, int.MaxValue, ErrorMessage = "is required")]
        public Departments Department { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "is required")]
        public List<AddEmployeeRoleModel> Role { get; set; }

        [DisplayName("Roles")]
        [Range(1, int.MaxValue, ErrorMessage = "is required")]
        public int RoleId { get; set; }

        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [Range(1, int.MaxValue, ErrorMessage = "is required")]
        public Location Location { get; set; }

        [DisplayName("Employee Code")]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [Range(1, 9999, ErrorMessage = ValidationMessages.LengthLimitForNumbers)]
        [Remote("IsEmployeeCodeUnique","Employee","Employee",ErrorMessage = ValidationMessages.UniqueValidationMessage)]
        [RegularExpression(ValidationRegex.EmployeeCodeFormat, ErrorMessage = ValidationMessages.EmployeeCodeErrorMessage)]
        public int EmployeeCode { get; set; }

        [DisplayName("Date Of Joining")]
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString, ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        public System.DateTime DateOfJoining { get; set; }

        [DisplayName("Reporting Manager")]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [Range(1, int.MaxValue, ErrorMessage = "is required")]
        public int ReportingManagerId { get; set; }

        public bool Trainee { get; set; }
        public bool IsReportingManager { get; set; }

        [DisplayName("Email Address")]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [EmailAddress(ErrorMessage = ValidationMessages.IncorrectFormat)]
        public string EmailAddress { get; set; }
       
        [DisplayName("Created By")]

        public int CreatedBy { get; set; }

        [DisplayName("Created Date")]
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString, ApplyFormatInEditMode = true)]
        public System.DateTime CreatedDate { get; set; }
        [RegularExpression(ValidationRegex.NameFormat, ErrorMessage = ValidationMessages.NameErrorMessage)]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [StringLength(45, ErrorMessage = ValidationMessages.LengthLimit)]
        public string Designation { get; set; }
        public bool IsDeactiavted { get; set; }

    }

    public class DisplayEmployeeListViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Department { get; set; }
        public int RoleId { get; set; }
        public Location Location { get; set; }
        public int EmployeeCode { get; set; }
        [DisplayFormat(DataFormatString = DisplayFormatConstants.DateFormatString, ApplyFormatInEditMode = true)]
        public System.DateTime DateOfJoining { get; set; }
        public int ReportingManagerId { get; set; }
        public Nullable<bool> Trainee { get; set; }
        public Nullable<bool> IsReportingManager { get; set; }
        public string Designation { get; set; }
        public Nullable<int> UserId { get; set; }
        public string ReportingManagerName { get; set; }
        public string Email { get; set; }
    }


    public class ReportingManagerModel 
    {
        public int Employee { get; set; }
        public List<EmployeeViewModel> ListOfReportees { get; set; }
    }
    
}