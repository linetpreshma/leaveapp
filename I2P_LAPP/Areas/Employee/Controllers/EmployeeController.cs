﻿using AutoMapper;
using Infinite.LAPP.Domain;
using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.Domain.IRepository;
using Infinite.LAPP.Domain.Service;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.Repository.QueryObjects;
using Infinite.LAPP.WebFront.Areas.Employee.Models;
using Infinite.LAPP.WebFront.Areas.Users.Models;
using Infinite.LAPP.WebFront.Controllers;
using Infinite.LAPP.WebFront.MVCInfrastructure.ClaimsSecurity;
using Infinite.LAPP.WebFront.MVCInfrastructure.IdentityFramework;
using Infinite.LAPP.WebFront.MVCInfratructure.MappingLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Infinite.LAPP.WebFront.Areas.Employee.Controllers
{
    public class EmployeeController : BaseController
    {
        private readonly CustomIdentityUserManager _userManager;
        private readonly IEmployeeRepository _empRep;
        private readonly EmployeeService _empService;
        private readonly EmployeeQuery _empQuery;
        private readonly ILeaveRepository _levRep;
        public EmployeeController(IEmployeeRepository empRep, CustomIdentityUserManager userManager, EmployeeService empService, EmployeeQuery empQuery, ILeaveRepository levRep)
        {
            if (empRep == null)
                throw new ArgumentNullException("empRep is null");
            if (userManager == null)
                throw new ArgumentNullException("userManager is null");
            if (empService == null)
                throw new ArgumentNullException("empService is null");
            if (empQuery == null)
                throw new ArgumentNullException("empQuery is null");
            if (levRep == null)
                throw new ArgumentNullException("levRep is null");
            _levRep = levRep;
            _empRep = empRep;
            _userManager = userManager;
            _empService = empService;
            _empQuery = empQuery;

        }

        [CustomClaimAttribute(SecurityAction.Demand, Resource = "User Management", Value = "canViewUser")]
        public ActionResult EmployeeList()
        {
            return View();
        }

        public JsonResult SearchEmployeeList(int locationId, bool Deactiavted)
        {
            var employees = Mapper.Map<List<vw_GetEmployeeList>, List<DisplayEmployeeListViewModel>>(_empQuery.GetAllEmployeeListDetails().Where(x => x.Location == locationId && x.IsDeactiavted == Deactiavted).ToList()).ToList();

            //return PartialView("SearchEmployeeList", employees);

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            return Json(employees, JsonRequestBehavior.AllowGet);
        }

        // GET: Employee/Employee
        [CustomClaimAttribute(SecurityAction.Demand, Resource = "User Management", Value = "canAddUser")]
        public ActionResult AddEmployee()
        {
            List<AddEmployeeRoleModel> roles = Mapper.Map<List<AddEmployeeRoleDomainModel>, List<AddEmployeeRoleModel>>(_empService.GetAllRolesForEmployee());
            var employee = new EmployeeViewModel() { Role = roles, DateOfJoining = DateTime.Now };
            ViewBag.ReportingManager = _empQuery.GetReportingManagers();

            return View(employee);
        }

        [HttpPost]
        public ActionResult AddEmployee(EmployeeViewModel employee)
        {
            var employees = Mapper.Map<EmployeeViewModel, EmployeeDomainModel>(employee);
            employee.IsDeactiavted = false;
            
             // hash password
            var passwordHash = _userManager.PasswordHasher.HashPassword(DomainConstants.Commonpassword);
            var user = new User()
            {
                EmailAddress = employee.EmailAddress,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                IsActive = true,
                Password = passwordHash,
                RoleId = employee.RoleId
            };
            int id = _empService.InsertEmployee(employees, user);
            //int id = _empRep.Insert(employees);

            return RedirectToAction("EmployeeList");
        }


        // GET: Employee/Employee/EditEmployee/5
        [CustomClaimAttribute(SecurityAction.Demand, Resource = "User Management", Value = "canEditUser")]
        public ActionResult EditEmployee(int id)
        {
            var employee = Mapper.Map<vw_GetEmployeeList, EmployeeViewModel>(_empQuery.GetAllEmployeeListDetails().FirstOrDefault(x=>x.Id == id));
            employee.Role = Mapper.Map<List<AddEmployeeRoleDomainModel>, List<AddEmployeeRoleModel>>(_empService.GetAllRolesForEmployee());
            ViewBag.ReportingManager = _empQuery.GetReportingManagers();
            return View(employee);
        }

       

        // POST: Employee/Employee/Update/5
        [HttpPost]
        public ActionResult EditEmployee(EmployeeViewModel employee)
        {
                var employees = Mapper.Map<EmployeeViewModel, EmployeeDomainModel>(employee);
                _empService.EditEmployee(employees);
                return RedirectToAction("EmployeeList");
          
        }

        public ActionResult DeactivateEmployee(bool isReportingManager,int empId)//EmployeeViewModel employee)
        {
            var employee = new ReportingManagerModel() { Employee = empId };
            if (isReportingManager)
            {
                employee.ListOfReportees = Mapper.Map<List<vw_GetEmployeeList>, List<EmployeeViewModel>>(_empQuery.GetAllEmployeeListDetails().Where(x => x.ReportingManagerId == empId).ToList());
                ViewBag.ReportingManager = _empQuery.GetReportingManagers();
                return View("ReselectReportingManager", employee);
            }
            else
            {
                _empRep.DeactivateEmployeeById(empId);
                return RedirectToAction("EmployeeList");
            }
        }

        [HttpPost]
        public ActionResult ReselectReportingManager(ReportingManagerModel model)
        {
              var employees = Mapper.Map<List<EmployeeViewModel>, List<EmployeeDomainModel>>(model.ListOfReportees);
              var id =_empService.EditListofEmployee(employees);
              _empRep.DeactivateEmployeeById(model.Employee);
              return RedirectToAction("EmployeeList");
        }


        public JsonResult IsEmployeeCodeUnique(int EmployeeCode)
        {
            var result = _empService.IsEmployeeCodeUnique(EmployeeCode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        
    }
}
