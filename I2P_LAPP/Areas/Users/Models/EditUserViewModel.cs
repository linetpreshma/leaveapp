﻿using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.WebFront.Common;
using Infinite.LAPP.WebFront.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Infinite.LAPP.WebFront.Areas.Users.Models
{
    public class EditUserViewModel : ClaimsModel
    {
        public int UserId { get; set; }

        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [StringLength(25, ErrorMessage = ValidationMessages.LengthLimit)]
        [Display(Name = "First Name")]
        [RegularExpression(ValidationRegex.NameFormat, ErrorMessage = ValidationMessages.NameErrorMessage)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [StringLength(25, ErrorMessage = ValidationMessages.LengthLimit)]
        [Display(Name = "Last Name")]
        [RegularExpression(ValidationRegex.NameFormat, ErrorMessage = ValidationMessages.NameErrorMessage)]
        public string LastName { get; set; }

        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Display(Name = "Role")]
        public int RoleId { get; set; }

        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [Display(Name = "Phone Number")]
        [RegularExpression(ValidationRegex.PhoneNo, ErrorMessage = ValidationMessages.PhoneNoErrorMessage)]
        public string PhoneNumber { get; set; }

        [Display(Name = "Active")]
        public bool IsActive { get; set; }

        public bool IsActiveOriginal { get; set; }

        public User ToDomainModel(EditUserViewModel model)
        {
            User user = new User();
            user.UserId = model.UserId;
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.EmailAddress = model.EmailAddress;
            user.RoleId = model.RoleId;
            user.IsActive = model.IsActive;
            return user;
        }
    }
}