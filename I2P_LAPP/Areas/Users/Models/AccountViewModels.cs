﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.WebFront.Common;

namespace Infinite.LAPP.WebFront.Areas.Users.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [Display(Name = "Email")]
        //[EmailAddress(ErrorMessage = ValidationMessages.IncorrectFormat)]
        public string Email { get; set; }

        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string PasswordHash { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
    public class ChangePasswordViewModel
    {
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        [System.Web.Mvc.Remote("CheckPasssword", "User", ErrorMessage = ValidationMessages.Invalid)]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [StringLength(20, MinimumLength = 8, ErrorMessage = ValidationMessages.PasswordLength)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        [RegularExpression(ValidationRegex.PasswordRegex, ErrorMessage = ValidationMessages.PasswordFormatErrorMessage)]
        [System.Web.Mvc.Remote("NotEqualToOldPassword", "User", AdditionalFields = "OldPassword", ErrorMessage = ValidationMessages.NewPassword)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = ValidationMessages.DoesNotMatch)]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        public string ConfirmPassword { get; set; }
    }
}
