﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Infinite.LAPP.WebFront.Models;

namespace Infinite.LAPP.WebFront.Areas.Users.Models
{
    public class DisplayUsersViewModel : ClaimsModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Role { get; set; }
        public int RoleId { get; set; }
        public string EmailAddress { get; set; }
    }
}