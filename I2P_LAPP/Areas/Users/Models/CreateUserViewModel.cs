﻿using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.WebFront.Common;
using Infinite.LAPP.WebFront.Models;
using System.ComponentModel.DataAnnotations;


namespace Infinite.LAPP.WebFront.Areas.Users.Models
{
    public class CreateUserViewModel : ClaimsModel
    {
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [StringLength(25, ErrorMessage = ValidationMessages.LengthLimit)]
        [Display(Name = "First Name")]
        [RegularExpression(ValidationRegex.NameFormat, ErrorMessage = ValidationMessages.NameErrorMessage)]
        public string FirstName { get; set; }

        [StringLength(25, ErrorMessage = ValidationMessages.LengthLimit)]
        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [Display(Name = "Last Name")]
        [RegularExpression(ValidationRegex.NameFormat, ErrorMessage = ValidationMessages.NameErrorMessage)]
        public string LastName { get; set; }

        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [EmailAddress(ErrorMessage = ValidationMessages.Invalid)]
        [System.Web.Mvc.Remote("IsEmailAvailable", "User", ErrorMessage = ValidationMessages.UniqueValidationMessage)]
        [StringLength(50, ErrorMessage = ValidationMessages.LengthLimit)]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Display(Name = "Role")]
        public string Role { get; set; }

        public int RoleId { get; set; }

        [Display(Name = "Active")]
        public bool IsActive { get; set; }

        [Required(ErrorMessage = ValidationMessages.RequiredValidationMessage)]
        [Display(Name = "Phone Number")]
        [RegularExpression(ValidationRegex.PhoneNo, ErrorMessage = ValidationMessages.PhoneNoErrorMessage)]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public User ToDomainModel(CreateUserViewModel model, string passwordHash)
        {
            User user = new User();
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.EmailAddress = model.EmailAddress;
            user.RoleId = model.RoleId;
            user.IsActive = model.IsActive;
            user.Password = passwordHash;
            user.Role = model.Role;
            return user;
        }
    }
}