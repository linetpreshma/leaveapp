﻿using Infinite.LAPP.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infinite.LAPP.WebFront.Areas.Users.Models
{
    public class DashboardDetailViewModel
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int EmployeeCode { get; set; }
        public System.DateTime Date { get; set; }
        public double LeavesBalanceLastMonth { get; set; }
        public double LeavesEarned { get; set; }
        public double LeavesTaken { get; set; }
        public double Shortfall { get; set; }
        public double AccuredLeaves { get; set; }
        public double LWP { get; set; }
        public double LeavesBalanceThisMonth { get; set; }

        public virtual EmployeeDetail EmployeeDetail { get; set; }
    }
}