﻿using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.WebFront.Common;
using Infinite.LAPP.WebFront.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Infinite.LAPP.WebFront.Areas.Users.Models
{
    public class RoleManagerViewModel : ClaimsModel
    {
        public List<RoleViewModel> RoleViewModelList { get; set; }
    }

    public class RoleViewModel
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public int UserCount { get; set; }
        public string RoleDescription { get; set; }
    }

    public class AddEmployeeRoleModel : RoleViewModel
    {
        public bool IsTrainee { get; set; }
        public bool IsReportingManager { get; set; }
    }

    public class CreateRoleViewModel : ClaimsModel
    {
        public int RoleId { get; set; }

        [Required]
        [Display(Name = "Role Name")]
        [Remote("IsUniqueRoleName", "RoleManager", AdditionalFields = "RoleId", ErrorMessage = "Role name already exists.")]
        [RegularExpression(ValidationRegex.NameFormatWithNumbers, ErrorMessage = ValidationMessages.NameNumberErrorMessage)]
        [StringLength(50, ErrorMessage = ValidationMessages.LengthLimit)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Display Name")]
        [Remote("IsUniqueDisplayRoleName", "RoleManager", AdditionalFields = "RoleId", ErrorMessage = ValidationMessages.UniqueValidationMessage)]
        [RegularExpression(ValidationRegex.NameFormatWithNumbers, ErrorMessage = ValidationMessages.NameNumberErrorMessage)]
        [StringLength(50, ErrorMessage = ValidationMessages.LengthLimit)]
        public string DisplayName { get; set; }

       
        public bool IsTrainee { get; set; }
        public bool IsReportingManager { get; set; }

        [Required]
        [Display(Name = "Role Description")]
        public string Description { get; set; }

        [Display(Name = "Role Permissions")]
        public List<RolePermissionViewModel> RolePermissionList { get; set; }

        public Permission ConvertToDomainModel()
        {
            var permission = new Permission
            {
                RoleId = RoleId,
                RoleName = Name,
                RoleDisplayName = DisplayName,
                RoleDescription = Description,
                IsTrainee =IsTrainee,
                IsReportingManager = IsReportingManager,
                Modules = new List<Module>()
            };
            if (RolePermissionList != null && RolePermissionList.Any())
            {
                foreach (var rolePermission in RolePermissionList)
                {
                    var module = new Domain.DomainModel.Module
                    {
                        ModuleId = rolePermission.ModuleId,
                        AccessRight = rolePermission.AccessRights,
                        ModuleName = rolePermission.ModuleName
                    };
                    permission.Modules.Add(module);
                }
            }
            return permission;
        }
    }

    public class EditRoleViewModel : CreateRoleViewModel
    {
        public bool IsPreDefinedRole { get; set; }
    }

    public class RolePermissionViewModel
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int ModuleId { get; set; }

        [Display(Name = "Module Name")]
        [RegularExpression(ValidationRegex.NameFormat, ErrorMessage = ValidationMessages.NameErrorMessage)]
        public string ModuleName { get; set; }

        public string AccessRights { get; set; }
    }
}