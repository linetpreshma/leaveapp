﻿using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.Domain.IRepository;
using Infinite.LAPP.WebFront.Areas.Users.Models;
using Infinite.LAPP.WebFront.Controllers;
using Infinite.LAPP.WebFront.MVCInfrastructure.ClaimsSecurity;
using Infinite.LAPP.WebFront.MVCInfrastructure.IdentityFramework;
using Infinite.LAPP.WebFront.MVCInfratructure.MappingLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web.Mvc;

namespace Infinite.LAPP.WebFront.Areas.Users.Controllers
{
     [Authorize]
    public class UserController : BaseController
    {
        private readonly CustomIdentityUserManager _userManager;
        private readonly IUserRepository _userRep;

        public UserController(CustomIdentityUserManager userManager, IUserRepository userRep)
        {
            if (userManager == null)
                throw new ArgumentNullException("userManager is null");
            if (userRep == null)
                throw new ArgumentNullException("userRep is null");

            this._userManager = userManager;
            this._userRep = userRep;
        }

        [CustomClaimAttribute(SecurityAction.Demand, Resource = "User Management", Value = "canViewUser")]
        [MappingFilterAttribute(typeof(List<User>), typeof(List<DisplayUsersViewModel>))]
        public ActionResult Index()
        {
            return View(_userRep.GetAll());
        }

        [CustomClaimAttribute(SecurityAction.Demand, Resource = "User Management", Value = "canAddUser")]
        [MappingFilterAttribute(typeof(User), typeof(CreateUserViewModel))]
        public ActionResult Create(int id = 0, string role = null)
        {
            ViewBag.RoleId = id;
            ViewBag.Role = role;

            ViewBag.Roles = new SelectList(_userRep.GetAllRoles(), "RoleId", "RoleName");
            User user = new User() { IsActive = true };
            return View(user);
        }

        [HttpPost]
        public ActionResult Create(CreateUserViewModel model)
        {
            // hash password
            var passwordHash = _userManager.PasswordHasher.HashPassword(model.Password);

            // save to db
            _userRep.Insert(model.ToDomainModel(model, passwordHash));

            return RedirectToAction("Index");
        }

        [CustomClaimAttribute(SecurityAction.Demand, Resource = "User Management", Value = "canEditUser")]
        [MappingFilterAttribute(typeof(User), typeof(EditUserViewModel))]
        public ActionResult Edit(int id)
        {
            var user = _userRep.GetById(id);
            ViewBag.Roles = new SelectList(_userRep.GetAllRoles(), "RoleId", "RoleName", user.RoleId);
            return View(user);
        }

        [HttpPost]
        public ActionResult Edit(EditUserViewModel model)
        {
            _userRep.Update(model.ToDomainModel(model));

            return RedirectToAction("Index");
        }

        [MappingFilterAttribute(typeof(User), typeof(EditUserProfileViewModel))]
        public ActionResult EditProfile()
        {
            return View(_userRep.GetById(base.LoggedInUserId));
        }

        [HttpPost]
        public ActionResult EditProfile(EditUserProfileViewModel model)
        {
            _userRep.UpdateProfile(model.ToDomainModel(model), base.LoggedInUserId);
            return RedirectToAction("Index", "DashBoard");
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            IdentityResult result = _userManager.ChangePassword(LoggedInUserId, model.OldPassword, model.NewPassword);

            if (result.Succeeded)
            {
                var userData = _userRep.GetById(LoggedInUserId);
            }
            return RedirectToAction("LogOff", "WebAccount");
        }

        [HttpGet]
        public virtual JsonResult IsEmailAvailable(CreateUserViewModel model)
        {
            return _userRep.IsEmailAvailable(model.EmailAddress)
                    ? Json(true, JsonRequestBehavior.AllowGet)
                    : Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult CheckPasssword(ChangePasswordViewModel model)
        {
            return this.IsPassswordValid(base.LoggedInUserId, model.OldPassword)
                   ? Json(true, JsonRequestBehavior.AllowGet)
                   : Json(false, JsonRequestBehavior.AllowGet);
        }

        public bool IsPassswordValid(int userId, string currentPassword)
        {
            PasswordVerificationResult result = _userManager.PasswordHasher.VerifyHashedPassword(_userRep.GetHashedPassword(userId), currentPassword);

            if (result.Equals(PasswordVerificationResult.Success))
                return true;
            return false;
        }

        [CustomClaimAttribute(SecurityAction.Demand, Resource = "User Management", Value = "canViewUser")]
        [MappingFilterAttribute(typeof(List<User>), typeof(List<DisplayUsersViewModel>))]
        public ActionResult _UserIndexView(int roleId = 0)
        {
            return View(_userRep.GetAll().Where(x => x.RoleId == roleId || roleId == 0).ToList());
        }

        [HttpGet]
        public JsonResult NotEqualToOldPassword(string NewPassword, string OldPassword)
        {
            var result = OldPassword == NewPassword ? false : true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}