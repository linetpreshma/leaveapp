﻿using AutoMapper;
using Infinite.LAPP.Domain.Service;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.Repository.QueryObjects;
using Infinite.LAPP.WebFront.Areas.Users.Models;
using Infinite.LAPP.WebFront.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Infinite.LAPP.WebFront.Areas.Users.Controllers
{
    public class DashboardController : BaseController
    {
        private EmployeeQuery _empQuery;
        private readonly EmployeeService _empService;

        public DashboardController(EmployeeQuery empQuery, EmployeeService empService)
        {
            if (empQuery == null)
                throw new ArgumentNullException("empQuery is null");
            _empQuery = empQuery;
            _empService = empService;
        }

         public ActionResult Index()
        {
            var employee = _empQuery.GetEmployeeDetailByUserId(base.LoggedInUserId);
             //dash service 
            //_empService.DashboardOperation(employee.Id);
           
            var dash = Mapper.Map<DashboardDetail, DashboardDetailViewModel>(_empQuery.GetDashboardDetails(employee.Id));
            if (dash == null)
            {
                dash= new DashboardDetailViewModel();
                dash.Date = DateTime.Now;
            }

            return View(dash);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}