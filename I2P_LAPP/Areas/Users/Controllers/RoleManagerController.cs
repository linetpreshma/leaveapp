﻿using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.Domain.IRepository;
using Infinite.LAPP.EntityModel;
using Infinite.LAPP.Repository.QueryObjects;
using Infinite.LAPP.WebFront.Areas.Users.Models;
using Infinite.LAPP.WebFront.Controllers;
using Infinite.LAPP.WebFront.MVCInfrastructure.ClaimsSecurity;
using Infinite.LAPP.WebFront.MVCInfratructure.MappingLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.IdentityModel.Services;


namespace Infinite.LAPP.WebFront.Areas.Users.Controllers
{
    [Authorize]
    public class RoleManagerController : BaseController
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IModuleRepository _moduleRepository;
        private readonly IRolePermissionRepository _rolePermissionRepository;
        private readonly ListRolesQuery _query;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleManagerController"/> class.
        /// </summary>
        /// <param name="roleRepository">The i role repository.</param>
        /// <param name="moduleRepository">The module repository.</param>
        /// <param name="rolePermissionRepository">The role permission repository.</param>
        public RoleManagerController(IRoleRepository roleRepository, IModuleRepository moduleRepository,
            IRolePermissionRepository rolePermissionRepository, ListRolesQuery query)
        {
            this._roleRepository = roleRepository;
            this._moduleRepository = moduleRepository;
            this._rolePermissionRepository = rolePermissionRepository;
            this._query = query;
        }

        /// <summary>
        /// This action method is used to get view for creating new role.
        /// </summary>
        /// <returns></returns>
        [CustomClaimAttribute(SecurityAction.Demand, Resource = "Role Management", Value = "canAddRole")]
        [MappingFilterAttribute(typeof(List<Module>), typeof(CreateRoleViewModel))]
        public ActionResult Create()
        {
            var moduleList = _moduleRepository.GetAll().ToList();

            return View(moduleList);
        }

        /// <summary>
        /// This action method is used to insert newly created role in DB along with it's access rights on modules.
        /// </summary>
        /// <param name="model">user role view model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Create(CreateRoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userRole = model.ConvertToDomainModel();
                _rolePermissionRepository.Insert(userRole);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        /// <summary>
        /// This action method is used to get all the roles present in DB.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomClaimAttribute(SecurityAction.Demand, Resource = "Role Management", Value = "canViewRole")]
        [MappingFilterAttribute(typeof(List<vw_RoleUserCount>), typeof(RoleManagerViewModel))]
        public ActionResult Index()
        {
            var roles = _query.Execute();

            return View(roles);
        }

        /// <summary>
        /// This action method is use to get view for editing existing role.
        /// </summary>
        /// <param name="id">role id</param>
        /// <returns></returns>
        [HttpGet]
        [CustomClaimAttribute(SecurityAction.Demand, Resource = "Role Management", Value = "canEditRole")]
        [MappingFilterAttribute(typeof(Permission), typeof(EditRoleViewModel))]
        public async Task<ActionResult> Edit(int id)
        {
            var userRole = _rolePermissionRepository.GetById(id);

            return View(userRole);
        }

        /// <summary>
        /// This action method is use to update an existing role record.
        /// </summary>
        /// <param name="model">existing user role</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Edit(EditRoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userRole = model.ConvertToDomainModel();
                _rolePermissionRepository.Update(userRole);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        [CustomClaimAttribute(SecurityAction.Demand, Resource = "Role Management", Value = "canViewRole")]
        [MappingFilterAttribute(typeof(Permission), typeof(EditRoleViewModel))]
        public ActionResult Profile(int id)
        {
            var userRole = _rolePermissionRepository.GetById(id);

            return View(userRole);
        }

        /// <summary>
        /// This method is used to check that the role name entered by the user in unique or not.
        /// </summary>
        /// <param name="roleName">role name</param>
        /// <param name="roleId">role id</param>
        /// <returns></returns>
        public JsonResult IsUniqueRoleName(string name, int roleId = 0)
        {
            var result = _rolePermissionRepository.IsValueUnique(
                Helpers.CommonMethods.GetPropertyName(() => new Permission().RoleName), name, roleId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method is used to check that the display role name entered by the user in unique or not.
        /// </summary>
        /// <param name="displayName">role display name</param>
        /// <param name="roleId">role id</param>
        /// <returns></returns>
        public JsonResult IsUniqueDisplayRoleName(string displayName, int roleId = 0)
        {
            var result = _rolePermissionRepository.IsValueUnique(
                Helpers.CommonMethods.GetPropertyName(() => new Permission().RoleDisplayName), displayName, roleId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

       
    }
}
