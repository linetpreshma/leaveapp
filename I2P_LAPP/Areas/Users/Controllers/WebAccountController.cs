﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Infinite.LAPP.WebFront;
using Infinite.LAPP.WebFront.Areas.Users.Models;
using Infinite.LAPP.WebFront.Controllers;
using System.Web.UI;
using Infinite.LAPP.WebFront.MVCInfrastructure.IdentityFramework;
using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;
using Infinite.LAPP.Domain.Service;
using Infinite.LAPP.WebFront.MVCInfratructure.MappingLayer;
using Infinite.LAPP.Domain.DomainModel;
using Infinite.LAPP.Repository;
using AutoMapper;
using Infinite.LAPP.Domain.IRepository;
using System.DirectoryServices;

namespace Infinite.LAPP.WebFront.Areas.Users.Controllers
{
    [Authorize]
    public class WebAccountController : BaseController
    {
        private IUserRepository _userRep;
        private CustomIdentitySignInManager _signInManager;
        private CustomIdentityUserManager _userManager;
        private CustomIdentityRoleManager _roleManager;
        private IAuthenticationManager _authenticationManager;
        private PermissionService _permissionService;


        public WebAccountController(CustomIdentityUserManager userManager, CustomIdentitySignInManager signInManager, IAuthenticationManager authenticationManager,
                                    PermissionService permissionService, CustomIdentityRoleManager roleManager, IUserRepository userRep)
        {
            if (userManager == null)
                throw new ArgumentNullException("userManager is null");
            if (signInManager == null)
                throw new ArgumentNullException("signInManager is null");
            if (authenticationManager == null)
                throw new ArgumentNullException("authenticationManager is null");
            if (permissionService == null)
                throw new ArgumentNullException("permissionService is null");
            if (roleManager == null)
                throw new ArgumentNullException("roleManager is null");
            if (userRep == null)
                throw new ArgumentNullException("userRep is null");

            _userManager = userManager;
            _signInManager = signInManager;
            _authenticationManager = authenticationManager;
            _permissionService = permissionService;
            _roleManager = roleManager;
            _userRep = userRep;

        }
        private bool HasPassword
        {
            get
            {
                var user = _userManager.FindById(base.LoggedInUserId);
                if (user != null)
                {
                    return user.PasswordHash != null;
                }
                return false;
            }
        }


        //
        // GET: /WebAccount/Login
        [AllowAnonymous]
        [OutputCache(NoStore = true, Location = OutputCacheLocation.None)]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Dashboard", new { area = "Users" });
            }
            else
            {
                ViewBag.ReturnUrl = returnUrl;
            }

            return View();
        }

        //
        // POST: /WebAccount/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            var result = SignInStatus.Failure;
            var userdata = _userRep.GetUserByUserName(model.Email);
            if (userdata!=null)
            {
                var username = userdata.FirstName + ' ' + userdata.LastName;


                // build UID string
                String CN = "cn=" + username + ",ou=Users,ou=Infinite-MDC,dc=ics,dc=global";
                // assign password

                // define LDAP connection
                DirectoryEntry root = new DirectoryEntry(
                    "LDAP://inmdcs0001.ics.global:389", CN, model.Password,
                    System.DirectoryServices.AuthenticationTypes.None);
                try
                {

                    // attempt to use LDAP connection
                    object connected = root.NativeObject;

                    // no exception, login successful
                    result = SignInStatus.Success;
                }
                catch (Exception ex)
                {
                    // exception thrown, login failed
                    result = SignInStatus.Failure;
                }
            }
            //comment
            //result = SignInStatus.Success;
            
           
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            //var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                     //return RedirectToAction("Index", "Dashboard");
                    return RedirectToAction("SetUserClaims", new { email = model.Email, returnUrl = returnUrl, password = model.Password });
                case SignInStatus.LockedOut:
                    ModelState.AddModelError("", "User locked Out");
                    return View();
                case SignInStatus.Failure:
                    ModelState.AddModelError("", "Invalid UserId/Password");
                    return View(model);
                case SignInStatus.RequiresVerification:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        /// <summary>
        /// Sets the user claims.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns></returns>
         [AllowAnonymous]
        public async Task<ActionResult> SetUserClaims(string email, string returnUrl, string password)
        {
            var userData = await _userManager.FindByNameAsync(email);
            if (userData == null) { userData = await _userManager.FindByEmailAsync(email); }
            var roleId = 0;
            if (userData.Roles.Any() && userData.Roles.FirstOrDefault() != null)
            {
                roleId = userData.Roles.FirstOrDefault().RoleId;
            }

            CustomIdentityRole role = null;

            if (roleId > 0)
            {
                role = _roleManager.FindById(roleId);
            }

            // Construct ApplicationUser model by getting details of the currently logged in user using OWIN.
            var user = new CustomIdentityUser { Id = userData.Id, UserName = email, Email = email };


            var claimsIdentity = await _signInManager.CreateUserIdentityAsync(user);

            #region Custom Implementation to fetch claims

            //custom implementation            
            List<Tuple<string, string>> configuredClaims = _permissionService.GetClaims(userData.Id);

            List<Claim> claims = new List<Claim>();
            foreach (var c in configuredClaims)
            {
                claims.Add(new Claim(c.Item1, c.Item2));
            }

            claims.Add(new Claim("role", Convert.ToString(role.Name)));
            claims.Add(new Claim("email", email));

            claimsIdentity.AddClaims(claims);
            #endregion

            _authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, claimsIdentity);

            //return RedirectToAction("Index", "DashBoard", new { area = "Users" });
            return RedirectToLocal(returnUrl);
        }

        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            _authenticationManager.SignOut();
            return RedirectToAction("Login");
        }


        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Dashboard");
        }

        #endregion
    }
}