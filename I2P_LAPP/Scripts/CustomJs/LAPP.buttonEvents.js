﻿$(document).ready(function () {
    var callBackUrl = '';

    //action button click event. all action button click event will come here. based on parameter renderPopUp
    //action result will show either on pop up or on new page. 
    $(document).off("click", "#actionButton");
    $(document).on("click", "#actionButton", function (e) {
        var url = this.dataset.url,
        renderPopUp = this.dataset.renderpopup;//set parameter to true if action result needs to be show on pop up.

        //// Call loader.js function to show loader
        //showLoader();

        if (renderPopUp) {
            callBackUrl = this.dataset.callbackurl;//need to reload the landing page if if we something on popup.
            var popupTitle = this.dataset.popuptitle;
            $.get(url, function (data) {

                //creating dialog object and button object array inside it.
                var dialogObject = {};
                dialogObject.buttonObject = [];
                dialogObject.displayMessgae = data;
                dialogObject.title = popupTitle;
                $.fn.renderActionModal(dialogObject, "renderFormModal");
            });
        }
        else {
            window.location.href = url;
        }
    });

    //cancel button click event for all cancel button. need to set parameter popupcancelbutton if the button 
    //is render on pop up.
    $(document).off("click", "#cancelButton");
    $(document).on("click", "#cancelButton", function (e) {
        var popupCancelButton = this.dataset.popupcancelbutton;

        if (popupCancelButton) {//if pop up cancel button click then only close the pop up.
            $("#closeIcon").trigger("click");
        } else {
            window.history.back();//go back to the previous page.
        }
    });

    //save button click event for all save buttons.need to set parameter popupsavebutton if the button 
    //is render on pop up.
    $(document).off("click", "#saveButton");
    $(document).on("click", "#saveButton", function (e) {
        $("form").valid();
        $("form").submit();
        //var settings = {};
        //settings.element = this;
        //callBackUrl = callBackUrl == undefined || callBackUrl == '' ? this.dataset != undefined ? this.dataset.redirecturl : '' : callBackUrl;//need to reload the landing page if we something on popup.
        //settings.callBackUrl = callBackUrl;
        //$.fn.saveRecordPlugin(settings);
        

    });

    //close icon button event. to close the pop up and reload the page.
    $(document).off("click", "#closeIcon");
    $(document).on("click", "#closeIcon", function (e) {
        $(".ui.modal").empty();

        var datePickerDiv = $("#ui-datepicker-div");
        if (datePickerDiv && datePickerDiv.length > 0) {
            datePickerDiv.remove();
        }

        var datetimePickerDiv = $(".xdsoft_datetimepicker");
        if (datetimePickerDiv && datetimePickerDiv.length > 0) {
            datetimePickerDiv.remove();
        }

        $(".ui.modal").modal('hide dimmer');

        if ($(e.target).data('closeiconurl') != undefined && $(e.target).data('closeiconurl') != '') {
            window.location.href = $(e.target).data('closeiconurl');
        }
    });

    //delete button click event for all delete buttons.need to set parameter popupsavebutton if the button 
    //is render on pop up.
    $(document).off("click", "#deleteButton");
    $(document).on("click", "#deleteButton", function (e) {
        var settings = {};
        settings.element = this;
        $.fn.deleteRecordPlugin(settings);
    });

    //use to show spinner on anchor tag click
    //$(document).off("click", "a");
    //$(document).on("click", "a", function (e) {
    //    if (enabledLoader && !$(e.target).data('hideloader')) {
    //        showLoader();
    //    }
    //});

    $(".btn-danger").hover(function () {
        $(this).children(".view-content").hide();
        $(this).children(".hide-content").show();
    },
    function () {
        $(this).children(".view-content").show();
        $(this).children(".hide-content").hide();
    }
    );

    $(".btn-success").hover(function () {
        $(this).children(".view-content").hide();
        $(this).children(".hide-content").show();
    },
   function () {
       $(this).children(".view-content").show();
       $(this).children(".hide-content").hide();
   }
   );

});

function DatesError() {
    $("#DatesError").dialog({
        autoOpen: true,
        resizable: false,
        title: 'Error',
        modal: true,
        open: function () {
            $(this).text("No Dates selected. Form cannot be submitted. Click Ok to Reload page ");
        },
        buttons: {
            "Ok": {
                click: function () {
                    window.location.reload();
                },
                class: "btn btn-primary",
                text: "Ok"
            }
        }
    });
    return false;
}
