﻿$(document).ready(function () {

});

function customDatetimePicker(currentSettings) {
    var defaultSettings = {
        minDate: false,
        maxDate: false,
        minTime: false,
        maxTime: false,
        format: 'd/M/y',
        value: new Date(),
        timepicker: false,
        autoSize: true,
        changeYear: true,
        changeMonth: true,
        showButtonPanel: true,
        showWeek: true,
        onDateSelect: function () { },
        onTimeSelect: function () { },
        onMonthChange: function () { },
        onYearChange: function () { },
        closeOnDateSelect: true,
        step: 30,
        showCalendar: true
    };

    var settings = $.extend(true, {}, defaultSettings, currentSettings);

    if ($.type(settings.minDate) === "number") {//if +- days condition 
        settings.minDate = new Date().setDate(new Date().getDate() + settings.minDate);
    }
    else if ($.type(currentSettings.minDate) === "date") {
        settings.minDate = currentSettings.minDate.getTime();
    }

    if (currentSettings.maxDate && $.type(currentSettings.maxDate) === "number") {
        settings.maxDate = new Date().setDate(new Date().getDate() + currentSettings.maxDate);
    }

    if (currentSettings.onDateSelect || settings.timepicker) {
        settings.onSelectDate = function (date, input, event) {
            var currentValue = this;
            DateChangeEvent(settings, date, input, event, currentValue);
        };
    }

    if (currentSettings.onMonthChange || settings.timepicker) {
        settings.onChangeMonth = function (date, input, event) {
            var currentValue = this;
            MonthChangeEvent(settings, date, input, event, currentValue);
        };
    }
    if (currentSettings.onYearChange || settings.timepicker) {
        settings.onChangeYear = function (date, input, event) {
            var currentValue = this;
            YearChangeEvent(settings, date, input, event, currentValue);
        };
    }
    if (currentSettings.onTimeSelect || settings.timepicker) {
        settings.onSelectTime = function (date, input, event) {
            var currentValue = this;
            TimeChangeEvent(settings, date, input, event, currentValue);
        };
    }
    if (settings.timepicker) {
        settings.format = dateTimeFormat;
        settings.minTime = true;
        if (currentSettings.minTime != undefined)
            settings.minTime = currentSettings.minTime;
    };


    if (!settings.showCalendar)//case when calender needs to hide so that user can select only month and year used in manage rates page.
    {
        settings.onChangeMonth = startDateYearChange;
        settings.onChangeYear = startDateYearChange;
    }

    $('#' + settings.id).datetimepicker(settings);
}


function DateChangeEvent(settings, date, input, event, currentValue) {
    var selectedDate = date;
    var minDate = new Date(settings.minDate);

    var newSettings = $.extend(true, {}, settings);

    if (selectedDate > minDate) {
        newSettings.minTime = false;
        if (selectedDate.getFullYear() == minDate.getFullYear() && selectedDate.getMonth() == minDate.getMonth() && selectedDate.getDate() == minDate.getDate()) {
            newSettings.minTime = settings.minTime;
        }
    }
    else {
        newSettings.minTime = settings.minTime;
    }

    currentValue.setOptions(newSettings);

    //call overload method if any in perticlar js file. 
    if (settings.onDateSelect && $.isFunction(settings.onDateSelect)) {
        settings.onDateSelect.call(settings, date, input, event, currentValue);
    }
}

function MonthChangeEvent(settings, date, input, event, currentValue) {
    var selectedDate = date;
    var minDate = new Date(settings.minDate);

    var newSettings = $.extend(true, {}, settings);

    if (selectedDate.getMonth() < minDate.getMonth() && selectedDate.getFullYear() == minDate.getFullYear()) {
        newSettings.timepicker = false;
    }
    else if (selectedDate.getFullYear() < minDate.getFullYear()) {
        newSettings.timepicker = false;
    }
    else {
        newSettings.timepicker = true;
        if (selectedDate > minDate) {
            newSettings.minTime = false;
        }
    }

    currentValue.setOptions(newSettings);

    //call overload method if any in perticlar js file. 
    if (settings.onMonthChange && $.isFunction(settings.onMonthChange)) {
        settings.onMonthChange.call(settings, date, input, event, currentValue);
    }
}

function YearChangeEvent(settings, date, input, event, currentValue) {
    var selectedDate = date;
    var minDate = new Date(settings.minDate);

    var newSettings = $.extend(true, {}, settings);

    if (selectedDate.getFullYear() < minDate.getFullYear()) {
        newSettings.timepicker = false;
    }
    else {
        newSettings.timepicker = true;
        if (selectedDate > minDate) {
            newSettings.minTime = false;
        }
    }

    currentValue.setOptions(newSettings);

    //call overload method if any in perticlar js file. 
    if (settings.onYearChange && $.isFunction(settings.onYearChange)) {
        settings.onYearChange.call(settings, date, input, event, currentValue);
    }
}

function TimeChangeEvent(settings, date, input, event, currentValue) {
    //call overload method if any in perticlar js file. 
    if (settings.onTimeSelect && $.isFunction(settings.onTimeSelect)) {
        settings.onTimeSelect.call(settings, date, input, event, currentValue);
    }
}

function startDateYearChange(date, control, event) {
    var selectedDateMonth = date.getMonth();
    selectedDateYear = date.getFullYear(),
    minDate = new Date(this.data().options.minDate),
    monthStart = minDate.getMonth();

    if (selectedDateYear > minDate.getFullYear()) {
        monthStart = 0;
    }
    else if (selectedDateMonth <= minDate.getMonth()) {
        selectedDateMonth = minDate.getMonth();
    }

    var DateToSet = new Date(selectedDateYear, selectedDateMonth, 1);
    this.setOptions({ value: DateToSet, monthStart: monthStart });
}