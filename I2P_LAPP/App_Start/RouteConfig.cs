﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Infinite.LAPP.WebFront
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { area = "Users", controller = "WebAccount", action = "Login", id = UrlParameter.Optional },
                constraints: null,
                namespaces: new[] { "Infinite.LAPP.WebFront.Areas.Users.Controllers" }
            ).DataTokens.Add("area", "Users");
        }
    }
}
