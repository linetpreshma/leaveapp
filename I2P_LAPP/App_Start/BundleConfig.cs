﻿using System.Web;
using System.Web.Optimization;

namespace Infinite.LAPP.WebFront
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqgrid").Include(
                        "~/Scripts/jquery.jqGrid.min.js",
                        "~/Scripts/i18n/grid.locale-en.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui")
                    .Include("~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/LAPPFunctions")
                    .Include("~/Scripts/CustomJs/LAPP.buttonEvents.js",
                    "~/Scripts/CustomJs/LAPP.CommonFunctions.js"));

            bundles.Add(new ScriptBundle("~/bundles/datetimepicker")
                    .Include("~/Scripts/DateTimePicker/jquery.datetimepicker.full.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/respond.js"));

            //style scripts

            bundles.Add(new StyleBundle("~/Content/csscl").Include(
                      "~/Content/CSS/bootstrap.min.css",
                       "~/Content/CSS/main-new.css",
                       "~/Content/ui.jqgrid.css"
                      ));
            bundles.Add(new StyleBundle("~/Content/baseui")
                   .Include("~/Content/themes/base/*.css"));
            bundles.Add(new StyleBundle("~/Content/jquerycss")
                  .Include("~/Content/jquery/*.css"));
        }
    }
}
