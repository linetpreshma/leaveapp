﻿using Infinite.LAPP.WebFront.MVCInfrastructure.ExceptionFilters;
using System.Web;
using System.Web.Mvc;

namespace Infinite.LAPP.WebFront
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new LoggingExceptionFilter());

            //filters.Add(new HandleErrorAttribute());

            filters.Add(new AuthorizeAttribute());
        }
    }
}
